/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sp_common.h"
#include "tmpm330_uart.h"
#include "tmpm330_cg.h"
#include "sp_debugcomm.h"
#include "sp_hostcomm.h"

#define TMRB_1ms (6000)

unsigned char hdmi_hotplug_mask  = 0;
unsigned char hdmi_state_changed = 0;
#if(defined(ENABLE_VGA))
unsigned char vga_state_changed  = 0;
unsigned char vga_hotplug_mask   = 0;
#endif
extern unsigned char tx_buffer[UART_RING_BUFFER_SIZE];
extern unsigned char sleep;
extern volatile SP_MPQ_STATE mpq_state;

void INT0_IRQHandler(void)
{
   hdmi_hotplug_mask = (0xF & GPIO_ReadData(GPIO_PJ));
   hdmi_state_changed = 1;
   CG_ClearINTReq(CG_INT_SRC_0);
}

void INT1_IRQHandler(void)
{
   hdmi_hotplug_mask = (0xF & GPIO_ReadData(GPIO_PJ));
   hdmi_state_changed = 1;
   CG_ClearINTReq(CG_INT_SRC_1);
}

void INT2_IRQHandler(void)
{
   hdmi_hotplug_mask = (0xF & GPIO_ReadData(GPIO_PJ));
   hdmi_state_changed = 1;
   CG_ClearINTReq(CG_INT_SRC_2);
}

void INT3_IRQHandler(void)
{
   hdmi_hotplug_mask = (0xF & GPIO_ReadData(GPIO_PJ));
   hdmi_state_changed = 1;
   CG_ClearINTReq(CG_INT_SRC_3);
}

void INT7_IRQHandler(void)
{
#if(defined(ENABLE_VGA))
   CG_ClearINTReq(CG_INT_SRC_7);
   vga_hotplug_mask  = GPIO_ReadDataBit(GPIO_PJ, GPIO_BIT_7);
   vga_state_changed = 1;
#endif
}

void SP_HandleMPQWakeup(void)
{
   switch (mpq_state)
   {
      case MPQ_STATE_POWERDOWN:
         SP_IssuePMICReset();
         break;
      case MPQ_STATE_SUSPEND:
         SP_WakeupMPQ();
         break;
   }
}

void SP_HandleVGAHotplug(void)
{
#if(defined(ENABLE_VGA))
   if (vga_state_changed == 1)
   {
      vga_state_changed = 0;
      /* Send Hotplug Event to Host */
      tx_buffer[HOSTCOMM_GROUP_INDEX]   = SP_GRP_HOTPLUG_DETECT;
      tx_buffer[HOSTCOMM_COMMAND_INDEX] = SP_VGA_HOTPLUG_EVENT;

      tx_buffer[HOSTCOMM_DATA_INDEX] = vga_hotplug_mask;
      SP_HostUARTSendData(sizeof(unsigned char) + 2);
   }
#endif
}

void SP_HandleHDMIHotplug(void)
{
   if (hdmi_state_changed == 1)
   {
      if (hdmi_hotplug_mask != 0x0)
      {
         SP_HandleMPQWakeup();
      }

      if (mpq_state == MPQ_STATE_POWERDOWN)
      {
         hdmi_state_changed = 0;
         return;
      }

      /* Send Hotplug Event to Host */
      tx_buffer[HOSTCOMM_GROUP_INDEX]   = SP_GRP_HOTPLUG_DETECT;
      tx_buffer[HOSTCOMM_COMMAND_INDEX] = SP_HDMI_HOTPLUG_EVENT;

      tx_buffer[HOSTCOMM_DATA_INDEX] = hdmi_hotplug_mask;

      SP_HostUARTSendData(sizeof(unsigned char) + 2);
      hdmi_state_changed = 0;
   }
}

void SP_ResetMCU(void)
{
   unsigned int reg_value = 0x0;
   reg_value  = SCB->AIRCR;
   reg_value  = 0x05FA0001;
   SCB->AIRCR = reg_value;
   __DSB();
   while(1);
}

void SP_FRCPowerOn(void)
{
   /* FRC_RST_N = 1 - FRC_PWR_EN_3V3 = 0 - FRC_UPDATE_N = 0 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_6, GPIO_BIT_VALUE_1);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_4, GPIO_BIT_VALUE_0);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_5, GPIO_BIT_VALUE_0);

   SP_WaitForDelay(1);

   /* FRC_RST_N = 0 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_6, GPIO_BIT_VALUE_0);

   SP_WaitForDelay(1);

   /* FRC_PWR_EN_3V3 = 1 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_4, GPIO_BIT_VALUE_1);

   SP_WaitForDelay(1);

   /* FRC_UPDATE_N = 1 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_5, GPIO_BIT_VALUE_1);

   SP_WaitForDelay(1);

   /* FRC_RST_N = 1 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_6, GPIO_BIT_VALUE_1);

   SP_WaitForDelay(1);
}

void SP_FRCPowerOff(void)
{
   /* FRC_RST_N = 0 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_6, GPIO_BIT_VALUE_0);

   SP_WaitForDelay(1);

   /* FRC_UPDATE_N = 0 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_5, GPIO_BIT_VALUE_0);

   SP_WaitForDelay(1);

   /* FRC_PWR_EN_3V3 = 0 */
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_4, GPIO_BIT_VALUE_0);

   SP_WaitForDelay(1);
}

void SP_ConfigGPIO(void)
{
   /* Pin Confgiuration for PWR_ON_SW_N - PH2 */
   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_2, GPIO_BIT_VALUE_1);
   GPIO_SetOutputEnableReg(GPIO_PH, GPIO_BIT_2, ENABLE);

   /* Pin Configuration for MCU_APQ_RESIN_N - PH3 */
   GPIO_DisableFuncReg(GPIO_PH, GPIO_FUNC_REG_1, GPIO_BIT_3);
   GPIO_SetOpenDrain(GPIO_PH, GPIO_BIT_3, ENABLE);
   GPIO_SetInputEnableReg(GPIO_PH, GPIO_BIT_3, ENABLE);

   /* Pin Configuration for 12V Check - PH3 */
   GPIO_SetInput(GPIO_PC, GPIO_BIT_1);
   GPIO_SetPullUp(GPIO_PC, GPIO_BIT_1, DISABLE);

   /* Pin Configuration for LED - PI0, PI1 */
   GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_0, ENABLE);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_0, GPIO_BIT_VALUE_0);

   GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_1, ENABLE);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_1, GPIO_BIT_VALUE_0);

   /* Pin Configuration for FRC - PI4/PI5/PI6 - FRC_PWR_EN_3V3/FRC_UPDATE_N/FRC_RST_N */
   GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_4, ENABLE);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_4, GPIO_BIT_VALUE_0);

   GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_5, ENABLE);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_5, GPIO_BIT_VALUE_0);

   GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_6, ENABLE);
   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_6, GPIO_BIT_VALUE_0);

   /* Output System Clock on PK1 - for debug */
   GPIO_SetOutputEnableReg(GPIO_PK, GPIO_BIT_1, ENABLE);
   GPIO_EnableFuncReg(GPIO_PK, GPIO_FUNC_REG_1, GPIO_BIT_1);
   CG_SetSCOUTSrc(CG_SCOUT_SRC_FSYS);

   /* Pin Configuration for HDMI/VGA Hot Plug Detection - PJ0-PJ3/PJ7 */
   GPIO_EnableFuncReg(GPIO_PJ, GPIO_FUNC_REG_1, GPIO_BIT_0);
   GPIO_EnableFuncReg(GPIO_PJ, GPIO_FUNC_REG_1, GPIO_BIT_1);
   GPIO_EnableFuncReg(GPIO_PJ, GPIO_FUNC_REG_1, GPIO_BIT_2);
   GPIO_EnableFuncReg(GPIO_PJ, GPIO_FUNC_REG_1, GPIO_BIT_3);
#if(defined(ENABLE_VGA))
   GPIO_EnableFuncReg(GPIO_PJ, GPIO_FUNC_REG_1, GPIO_BIT_7);
#endif

   GPIO_SetInputEnableReg(GPIO_PJ, GPIO_BIT_0, ENABLE);
   GPIO_SetInputEnableReg(GPIO_PJ, GPIO_BIT_1, ENABLE);
   GPIO_SetInputEnableReg(GPIO_PJ, GPIO_BIT_2, ENABLE);
   GPIO_SetInputEnableReg(GPIO_PJ, GPIO_BIT_3, ENABLE);
#if(defined(ENABLE_VGA))
   GPIO_SetInputEnableReg(GPIO_PJ, GPIO_BIT_7, ENABLE);
#endif

   INT_LOCK();

   CG_ClearINTReq(CG_INT_SRC_0);
   CG_ClearINTReq(CG_INT_SRC_1);
   CG_ClearINTReq(CG_INT_SRC_2);
   CG_ClearINTReq(CG_INT_SRC_3);
#if(defined(ENABLE_VGA))
   CG_ClearINTReq(CG_INT_SRC_7);
#endif

   /* Configure Interrupts for Hot Plug Detection Pins to Both Edges */
   CG_SetSTBYReleaseINTSrc(CG_INT_SRC_0, CG_INT_ACTIVE_STATE_BOTH_EDGES, ENABLE);
   CG_SetSTBYReleaseINTSrc(CG_INT_SRC_1, CG_INT_ACTIVE_STATE_BOTH_EDGES, ENABLE);
   CG_SetSTBYReleaseINTSrc(CG_INT_SRC_2, CG_INT_ACTIVE_STATE_BOTH_EDGES, ENABLE);
   CG_SetSTBYReleaseINTSrc(CG_INT_SRC_3, CG_INT_ACTIVE_STATE_BOTH_EDGES, ENABLE);
#if(defined(ENABLE_VGA))
   CG_SetSTBYReleaseINTSrc(CG_INT_SRC_7, CG_INT_ACTIVE_STATE_BOTH_EDGES, ENABLE);
#endif

   NVIC_Enable_Interrupt(INT0_interrupt);
   NVIC_Enable_Interrupt(INT1_interrupt);
   NVIC_Enable_Interrupt(INT2_interrupt);
   NVIC_Enable_Interrupt(INT3_interrupt);
#if(defined(ENABLE_VGA))
   NVIC_Enable_Interrupt(INT7_interrupt);
#endif

   /* PH0 - Switch to TB0IN0 */
   GPIO_SetOpenDrain(GPIO_PH, GPIO_BIT_0, ENABLE);
   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_0, GPIO_BIT_VALUE_1);
   GPIO_SetInputEnableReg(GPIO_PH, GPIO_BIT_0, ENABLE);
   GPIO_EnableFuncReg(GPIO_PH, GPIO_FUNC_REG_1, GPIO_BIT_0);

   /* Set PH1 as Input for MPQMCU_ACK */
   GPIO_SetOpenDrain(GPIO_PH, GPIO_BIT_1, ENABLE);
   GPIO_SetInputEnableReg(GPIO_PH, GPIO_BIT_1, ENABLE);
   GPIO_EnableFuncReg(GPIO_PH, GPIO_FUNC_REG_1, GPIO_BIT_1);

   INT_FREE();
}

void SP_WaitForDelay(unsigned int delay_ms)
{
   TMRB_InitTypeDef myTMRB;
   TMRB_FFOutputTypeDef ffopTMRB;

   myTMRB.Mode = TMRB_INTERVAL_TIMER;
   myTMRB.ClkDiv = TMRB_CLK_DIV_8;
   myTMRB.Cycle = TMRB_1ms;    /* Specific value depends on system clock */
   myTMRB.UpCntCtrl = TMRB_FREE_RUN;
   myTMRB.Duty = TMRB_1ms / 2U;        /* Specific value depends on system clock */

   TMRB_Enable(TSB_TB1);
   TMRB_SetRunState(TSB_TB1, TMRB_STOP);
   TMRB_SetDoubleBuf(TSB_TB1, DISABLE);
   TMRB_Init(TSB_TB1, &myTMRB);

   ffopTMRB.FlipflopCtrl = TMRB_FLIPFLOP_CLEAR;
   ffopTMRB.FlipflopReverseTrg = (TMRB_FLIPFLOP_MATCH_CYCLE | TMRB_FLIPFLOP_MATCH_DUTY);
   TMRB_SetFlipFlop(TSB_TB1, &ffopTMRB);
   TMRB_SetCaptureTiming(TSB_TB1, TMRB_DISABLE_CAPTURE);

   TMRB_SetRunState(TSB_TB1, TMRB_RUN);

   /* Wait for the timer to stop */
   while ((TSB_TB1->ST & 0x2) == 0);

   TMRB_SetRunState(TSB_TB1, TMRB_STOP);
   TMRB_Disable(TSB_TB1);
}

volatile unsigned short count  = 0;
void INTCAP00_IRQHandler()
{
   count++;
}

void INTCAP01_IRQHandler()
{
   if (count == 1)
   {
      if (mpq_state == MPQ_STATE_ACTIVE)
      {
         mpq_state = MPQ_STATE_SUSPEND;
      }
      else if (mpq_state == MPQ_STATE_SUSPEND)
      {
         mpq_state = MPQ_STATE_ACTIVE;
      }
   }
   else if (count == 2)
   {
      mpq_state = MPQ_STATE_POWERDOWN;
   }
   count = 0;
}

void SP_TimerCaptureInit(void)
{
   TMRB_InitTypeDef myTMRB;

   myTMRB.Mode = TMRB_INTERVAL_TIMER;
   myTMRB.ClkDiv = TMRB_CLK_DIV_8;
   myTMRB.Cycle = 6;
   myTMRB.UpCntCtrl = TMRB_FREE_RUN;
   myTMRB.Duty = 3;

   TMRB_Enable(TSB_TB0);
   TMRB_SetRunState(TSB_TB0, TMRB_STOP);
   TMRB_SetDoubleBuf(TSB_TB0, DISABLE);
   TMRB_Init(TSB_TB0, &myTMRB);

   TMRB_SetCaptureTiming(TSB_TB0, TMRB_CAPTURE_IN_RISING);

   NVIC_Enable_Interrupt(INTCAP00_interrupt);
   NVIC_Enable_Interrupt(INTCAP01_interrupt);

   TMRB_SetRunState(TSB_TB0, TMRB_RUN);
}

void SP_HardwareInit(void)
{
   SystemInit();

   SP_ConfigGPIO();

   SP_TimerCaptureInit();
}

void SP_IssuePMICReset(void)
{
   int i = 0;

   /* Pin Configuration for PM8920_RESIN_N - PH4 */
   GPIO_SetOutputEnableReg(GPIO_PH, GPIO_BIT_4, DISABLE);
   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_4, GPIO_BIT_VALUE_1);
   GPIO_SetOutputEnableReg(GPIO_PH, GPIO_BIT_4, ENABLE);
   for (i = 0; i < 40; i++)
     SP_WaitForDelay(1);

   /* Set the GPIO-PH2 to Low */
   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_2, GPIO_BIT_VALUE_0);

   /* Hold it low for some time */
   for (i = 0; i < 1000; i++)
     SP_WaitForDelay(1);

   /* Set the GPIO-PH2 to High */
   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_2, GPIO_BIT_VALUE_1);
}

void SP_PowerOffPMIC(void)
{
   int i;

   /* Set the GPIO-PH4 to Low */
   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_4, GPIO_BIT_VALUE_0);

   /* Hold it Low for some time */
   for (i = 0; i < 13000; i++)
   SP_WaitForDelay(1);

   /* Set the GPIO-PH4 to High */
   GPIO_WriteDataBit(GPIO_PH, GPIO_BIT_4, GPIO_BIT_VALUE_1);
}

void SIO_Configuration(TSB_SC_TypeDef * SCx)
{
   if (SCx == UART0)
   {
      TSB_PE_CR_PE0C = 1U;
      TSB_PE_FR1_PE0F1 = 1U;
      TSB_PE_FR1_PE1F1 = 1U;
      TSB_PE_IE_PE1IE = 1U;
   }
   else if (SCx == UART1)
   {
      TSB_PE_CR_PE4C = 1U;
      TSB_PE_FR1_PE4F1 = 1U;
      TSB_PE_FR1_PE5F1 = 1U;
      TSB_PE_IE_PE5IE = 1U;
   }
   else if (SCx == UART2)
   {
     TSB_PF_CR_PF0C = 1U;
     TSB_PF_FR1_PF0F1 = 1U;
     TSB_PF_FR1_PF1F1 = 1U;
     TSB_PF_IE_PF1IE = 1U;
   }
}
