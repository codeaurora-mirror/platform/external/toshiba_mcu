/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sp_ir.h"
#include "sp_common.h"

extern unsigned char IRFrameReceived;
extern unsigned int  IR_Command;

#if(defined(RC5_FORMAT_RMC))
void INTRMCRX1_IRQHandler(void)
{
   RMC_INTFactor myRMC_INTFactor;
   RMC_RxDataTypeDef myRMC_RxDataDef;

   static unsigned char toggle = OFF;

   myRMC_INTFactor = RMC_GetINTFactor(TSB_RMC1);

   if (myRMC_INTFactor.Bit.LowWidthDetection)
   {
      /* Low width detection interrupt factor */
      myRMC_RxDataDef = RMC_GetRxData(TSB_RMC1);

      /* 14 bits data received */
      if (myRMC_RxDataDef.RxDataBits == 0x0EU)
      {
         IRFrameReceived = TRUE;
         IR_Command = myRMC_RxDataDef.RxBuf1;

         toggle = (toggle)?(OFF):(ON);
      }
   }

   /* Clear RMC1 INT */
   TSB_CG->ICRCG = 0x0BU;
}
#endif

#if(defined(SAMSUNG_NEC_RMC))
unsigned int SP_ConvertNECxScancode(unsigned int x)
{
   __asm
   {
      RBIT x,x;
      REV  x,x;
   }

   return (x >> 8);
}

void INTRMCRX1_IRQHandler(void)
{
   RMC_INTFactor myRMC_INTFactor;
   RMC_RxDataTypeDef myRMC_RxDataDef;
   RMC_LeaderDetection myRMC_LeaderDetection;

   myRMC_INTFactor = RMC_GetINTFactor(TSB_RMC1);

   if (myRMC_INTFactor.Bit.MaxDataBitCycle)
   {
      myRMC_LeaderDetection = RMC_GetLeader(TSB_RMC1);

      if (myRMC_LeaderDetection == RMC_LEADER_DETECTED)
      {
         myRMC_RxDataDef = RMC_GetRxData(TSB_RMC1);
         if (myRMC_RxDataDef.RxDataBits == 0x20)
         {
            IRFrameReceived = TRUE;
            IR_Command = SP_ConvertNECxScancode(myRMC_RxDataDef.RxBuf1);
         }
      }
   }

   /* Clear RMC1 INT */
   TSB_CG->ICRCG = 0x0BU;
}
#endif

static void RMC_Configuration(TSB_RMC_TypeDef * RMCx)
{
   if (RMCx == TSB_RMC0)
   {
      TSB_PE_FR1_PE3F1 = 1;
      TSB_PE_IE_PE3IE = 1;
   }
   else if (RMCx == TSB_RMC1)
   {
      TSB_PF_FR1_PF3F1 = 1;
      TSB_PF_IE_PF3IE = 1;
   }
}

void SP_IRInit(void)
{
   RMC_InitTypeDef userRMC;
   TSB_RMC_TypeDef *RMCx = TSB_RMC1;

   RMC_Configuration(RMCx);

   INT_LOCK();

   TSB_CG->IMCGC = 0x30000000U;
   TSB_CG->IMCGC = 0x31000000U;
   TSB_CG->ICRCG = 0x0BU;

#if(defined(RC5_FORMAT_RMC))
   userRMC.LeaderPara.MaxCycle = RMC_MAX_CYCLE;
   userRMC.LeaderPara.MinCycle = RMC_MIN_CYCLE;
   userRMC.LeaderPara.MaxLowWidth = RMC_MAX_LOW_WIDTH;
   userRMC.LeaderPara.MinLowWidth = RMC_MIN_LOW_WIDTH;
   userRMC.LeaderPara.LeaderDetectionState = DISABLE;
   userRMC.LeaderPara.LeaderINTState = DISABLE;
   userRMC.FallingEdgeINTState = DISABLE;
   userRMC.SignalRxMethod = RMC_RX_IN_PHASE_METHOD;
   userRMC.LowWidth = RMC_TRG_LOW_WIDTH;
   userRMC.MaxDataBitCycle = RMC_TRG_MAX_DATA_BIT_CYCLE;
   userRMC.LargerThreshold = RMC_LARGER_THRESHOLD;
   userRMC.SmallerThreshold = RMC_SMALLER_THRESHOLD;
   userRMC.InputSignalReversedState = ENABLE;
   userRMC.NoiseCancellationTime = RMC_NOISE_CANCELLATION_TIME;
#endif

#if(defined(SAMSUNG_NEC_RMC))
    userRMC.LeaderPara.MaxCycle = RMC_MAX_CYCLE;
    userRMC.LeaderPara.MinCycle = RMC_MIN_CYCLE;
    userRMC.LeaderPara.MaxLowWidth = RMC_MAX_LOW_WIDTH;
    userRMC.LeaderPara.MinLowWidth = RMC_MIN_LOW_WIDTH;
    userRMC.LeaderPara.LeaderDetectionState = ENABLE;
    userRMC.LeaderPara.LeaderINTState = DISABLE;
    userRMC.FallingEdgeINTState = DISABLE;
    userRMC.SignalRxMethod = RMC_RX_IN_CYCLE_METHOD;
    userRMC.LowWidth = RMC_TRG_LOW_WIDTH;
    userRMC.MaxDataBitCycle = RMC_TRG_MAX_DATA_BIT_CYCLE;
    userRMC.LargerThreshold = RMC_LARGER_THRESHOLD;
    userRMC.SmallerThreshold = RMC_SMALLER_THRESHOLD;
    userRMC.InputSignalReversedState = DISABLE;
    userRMC.NoiseCancellationTime = RMC_NOISE_CANCELLATION_TIME;
#endif

   /* Enable and config the function for RMC channel */
   RMC_Enable(RMCx);
   RMC_SetIdleMode(RMCx, ENABLE);
   RMC_Init(RMCx, &userRMC);

   /* RMC Reception enable */
   RMC_SetRxCtrl(RMCx, ENABLE);

   /* Enable the Receive interrupt for RMC channel */
   NVIC_Enable_Interrupt(INTRMCRX1_interrupt);

   INT_FREE();
}
