/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <assert.h>
#include <pthread.h>

#include <mpq_standby_utils.h>

#define MPQ_SP_DLOAD_IMAGE    "/system/bin/dload.bin"
#define MPQ_SP_APPS_IMAGE     "/system/bin/standby.bin"
#define MPQ_DLOAD_START_ADDR  0x20001000
#define MPQ_DLOAD_BUFFER_SIZE 512
#define MPQ_SP_MAX_TIMEOUTS   600

static int uart_fd;
static FILE* pDloadImage;
static FILE* pAppsImage;
static unsigned short image_size;

static int mpq_stand_dload_image_term(void)
{
   if (pDloadImage)
   {
      fclose(pDloadImage);
   }

   if (pAppsImage)
   {
      fclose(pAppsImage);
   }

   image_size  = 0x00;
   pAppsImage  = NULL;
   pDloadImage = NULL;

   return 0;
}

static int mpq_stand_dload_image_init(void)
{
   int ret = 0;
   long size = 0;

   pDloadImage = NULL;
   pAppsImage  = NULL;
   image_size  = 0x00;

   pDloadImage = fopen(MPQ_SP_DLOAD_IMAGE, "rb");
   if (NULL == pDloadImage)
   {
      MPQ_SP_DBG_MSG("Failed to open %s\n", MPQ_SP_DLOAD_IMAGE);
      ret = -EBADF;
      goto cleanup;
   }

   pAppsImage  = fopen(MPQ_SP_APPS_IMAGE, "rb");
   if (NULL == pAppsImage)
   {
      MPQ_SP_DBG_MSG("Failed to open %s\n", MPQ_SP_APPS_IMAGE);
      ret = -EBADF;
      goto cleanup;
   }

   /* Estimate Image Size */
   fseek(pDloadImage, 0x00, SEEK_END);
   size = ftell(pDloadImage);
   fseek(pDloadImage, 0x00, SEEK_SET);
   image_size = (unsigned short)size;

   return 0;

cleanup:
   mpq_stand_dload_image_term();
   return ret;
}

static int mpq_standby_dload_uart_init(void)
{
   int ret = 0;
   struct termios options;

   memset(&uart_fd, 0x00, sizeof(int));

   uart_fd = open(STANDBY_UART_DEV, O_RDWR | O_NOCTTY | O_NDELAY);
   if (uart_fd < 0)
   {
      MPQ_SP_DBG_MSG("Failed to open UART device : %d\n", uart_fd);
      return -ENODEV;
   }

   ret = tcflush(uart_fd, TCIOFLUSH);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in tcflush with %d\n", ret);
      goto cleanup;
   }

   ret = tcgetattr(uart_fd, &options);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in tcgetattr with %d\n", ret);
      goto cleanup;
   }

   options.c_cc[VTIME]  = 0; /* inter-character timer unused */
   options.c_cc[VMIN]   = 1; /* blocking read until 5 chars received */
   options.c_cflag     &= ~CSIZE;
   options.c_cflag     &= ~CRTSCTS;
   options.c_cflag     &= ~CSTOPB;
   options.c_cflag     |= (CS8 | CLOCAL | CREAD);
   options.c_iflag      = IGNPAR;
   options.c_oflag      = 0;
   options.c_lflag      = 0;
   cfsetospeed(&options, B115200);
   cfsetispeed(&options, B115200);

   ret = tcsetattr(uart_fd, TCSANOW, &options);
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in tcsetattr with %d\n", ret);
      goto cleanup;
   }

   fcntl(uart_fd, F_SETFL, 0);

   MPQ_SP_DBG_MSG("UART Initialization for Download complete\n");

   return 0;

cleanup:
   close(uart_fd);
   return ret;
}

static int mpq_standby_uart_write(unsigned char *buffer, size_t size)
{
   size_t bytes_ret = 0;

   bytes_ret = write(uart_fd, buffer, size);
   if (bytes_ret != size)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_uart_write:: Wrote : %d" \
                     "Out of %d\n", bytes_ret, size);
      return -EIO;
   }

   return 0;
}

static int mpq_standby_timed_uart_read(unsigned char *buffer, size_t size)
{
   size_t bytes_ret = 0;
   fd_set infids;
   struct timeval tv;
   int ret = 0;

   FD_ZERO (&infids);
   FD_SET (uart_fd, &infids);
   tv.tv_sec  = 1;
   tv.tv_usec = 0;
   ret = select(uart_fd + 1, &infids, NULL, NULL, &tv);
   if (ret < 0)
   {
      MPQ_SP_DBG_MSG("Failed in select() : %d\n", ret);
      return -EIO;
   }
   else if (ret == 0)
   {
      return -ETIMEDOUT;
   }

   bytes_ret = read(uart_fd, buffer, size);
   if (bytes_ret != size)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_uart_read:: Read : %d" \
                     " Out of %d\n", bytes_ret, size);
      return -EIO;
   }

   return 0;
}

static int mpq_standby_uart_read(unsigned char *buffer, size_t size)
{
   size_t bytes_ret = 0;

   bytes_ret = read(uart_fd, buffer, size);
   if (bytes_ret != size)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_uart_read:: Read : %d" \
                     "Out of %d\n", bytes_ret, size);

      return -EIO;
   }

   return 0;
}

static int mpq_standby_dload_init(void)
{
   int ret = 0;

   /* Set Boot Pin to Low */
   ret = mpq_standby_mcu_gpio_ctrl(SET_MCU_BOOT_DLOAD);
   if (ret)
   {
      MPQ_SP_DBG_MSG("mpq_standby_mcu_gpio_ctrl failed with %d\n", ret);
      goto cleanup;
   }

   /* Initialize the UART */
   ret = mpq_standby_dload_uart_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("mpq_standby_uart_init failed with %d\n", ret);
      goto cleanup;
   }

cleanup:
   return ret;
}

static int mpq_standby_dload_term(void)
{
   int ret = 0;

   /* Terminate UART Connection */
   if (uart_fd)
   {
      close(uart_fd);
      uart_fd = 0;
   }

cleanup:
   return ret;
}

static unsigned char mpq_standby_calc_checksum(const unsigned char *buffer,
                                        size_t size)
{
   unsigned char checksum = 0;
   size_t i = 0;
   for (i = 0; i < size; i++)
   {
      checksum += buffer[i];
   }
   checksum = 0 - checksum;

   return checksum;
}

int mpq_standby_erase_flash(void)
{
   int ret = 0;
   unsigned char cmd = 0;
   unsigned char ack = 0;
   int fail_count = 0;

   ret = mpq_standby_dload_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_dload_init with %d\n", ret);
      goto cleanup;
   }

   do
   {
      /* Switch to DLOAD Mode */
      cmd = 0x86;
      ret = mpq_standby_uart_write(&cmd, sizeof(char));

      ack = 0x00;
      ret = mpq_standby_timed_uart_read(&ack, sizeof(char));

      if (ret == -ETIMEDOUT)
      {
         fail_count++;
         MPQ_SP_DBG_MSG("UART Read TIMED OUT %d\n", fail_count);
      }
      else if (ack != 0x86)
      {
         MPQ_SP_DBG_MSG("Failed to Switch MCU to ERASE Mode :"\
                        "ACK 0x%02x\n", ack);
         ret = -EIO;
         goto cleanup;
      }
      else
      {
         break;
      }
   } while(fail_count < MPQ_SP_MAX_TIMEOUTS);

   if (fail_count == MPQ_SP_MAX_TIMEOUTS)
   {
      MPQ_SP_DBG_MSG("Failed to get response from MCU\n");
      ret = -EIO;
      goto cleanup;
   }

   /* Send Flash Erase Command Code */
   cmd = 0x40;
   ret = mpq_standby_uart_write(&cmd, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x40)
   {
      MPQ_SP_DBG_MSG("Failed in Flash Erase Command : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   /* Send Chip Erase Command Code */
   cmd = 0x54;
   ret = mpq_standby_uart_write(&cmd, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x54)
   {
      MPQ_SP_DBG_MSG("Failed in Chip Erase Command : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x4F)
   {
      MPQ_SP_DBG_MSG("Failed in Chip Erase Command : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   usleep(10000);
   MPQ_SP_DBG_MSG("MCU Chip Erase Complete.\n");
cleanup:


   mpq_standby_dload_term();
   return ret;
}

char term_str[10] =  "TERMINATE";

int mpq_standby_upgrade_fw(void)
{
   int ret = 0;
   int bytes_ret = 0;
   unsigned char *pDloadBuffer = NULL;
   unsigned char *pAppsBuffer  = NULL;
   unsigned char cmd = 0;
   unsigned char ack = 0;
   unsigned char checksum = 0;
   unsigned char loop_here = TRUE;
   unsigned char passwd[12];
   unsigned int base_addr = MPQ_DLOAD_START_ADDR;
   unsigned int swapped_base_addr = 0x00;
   unsigned short swapped_image_size = 0x00;
   int fail_count = 0;

   ret = mpq_stand_dload_image_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_stand_dload_image_init with %d\n", ret);
      goto cleanup;
   }

   pDloadBuffer = (unsigned char *)malloc(image_size * sizeof(char));
   if (NULL == pDloadBuffer)
   {
      MPQ_SP_DBG_MSG("Failed to allocate DLOAD Buffer\n");
      ret = -ENOMEM;
      goto cleanup;
   }

   pAppsBuffer = (unsigned char *)malloc(MPQ_DLOAD_BUFFER_SIZE * sizeof(char));
   if (NULL == pAppsBuffer)
   {
      MPQ_SP_DBG_MSG("Failed to allocate DLOAD IMAGE Buffer\n");
      ret = -ENOMEM;
      goto cleanup;
   }

   memset(pDloadBuffer, 0x00, image_size * sizeof(char));
   bytes_ret = fread(pDloadBuffer, sizeof(char),
                     image_size * sizeof(char), pDloadImage);
   if (bytes_ret != image_size)
   {
      MPQ_SP_DBG_MSG("Failed to Read data from DLOAD image: Read : %d"\
                     "Image Size : %d\n", bytes_ret, image_size);
      ret = -EIO;
      goto cleanup;
   }

   ret = mpq_standby_dload_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_dload_init with %d\n", ret);
      goto cleanup;
   }

   do
   {
      /* Switch to DLOAD Mode */
      cmd = 0x86;
      ret = mpq_standby_uart_write(&cmd, sizeof(char));

      ack = 0x00;
      ret = mpq_standby_timed_uart_read(&ack, sizeof(char));

      if (ret == -ETIMEDOUT)
      {
         fail_count++;
         MPQ_SP_DBG_MSG("UART Read TIMED OUT %d\n", fail_count);
      }
      else if (ack != 0x86)
      {
         MPQ_SP_DBG_MSG("Failed to Switch MCU to DLOAD Mode :"\
                        "ACK 0x%02x\n", ack);
         ret = -EIO;
         goto cleanup;
      }
      else
      {
          break;
      }
   } while(fail_count < MPQ_SP_MAX_TIMEOUTS);

   if (fail_count == MPQ_SP_MAX_TIMEOUTS)
   {
      MPQ_SP_DBG_MSG("Failed to get response from MCU\n");
      ret = -EIO;
      goto cleanup;
   }

   /* Send Flash Erase Command Code */
   cmd = 0x40;
   ret = mpq_standby_uart_write(&cmd, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x40)
   {
      MPQ_SP_DBG_MSG("Failed in Flash Erase Command : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   /* Send Chip Erase Command Code */
   cmd = 0x54;
   ret = mpq_standby_uart_write(&cmd, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x54)
   {
      MPQ_SP_DBG_MSG("Failed in Chip Erase Command : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x4F)
   {
      MPQ_SP_DBG_MSG("Failed in Chip Erase Command : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   usleep(10000);
   MPQ_SP_DBG_MSG("MCU Chip Erase Complete.\n");

   /* Send IRAM Tx Command Code */
   cmd = 0x10;
   ret = mpq_standby_uart_write(&cmd, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x10)
   {
      MPQ_SP_DBG_MSG("Failed in IRAM Tx Command : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   /* Send 12 Byte Password + 1 Byte checksum */
   checksum = 0;
   memset(passwd, 0xFF, 12*sizeof(char));
   checksum = mpq_standby_calc_checksum(passwd, 12 * sizeof(char));
   ret = mpq_standby_uart_write(passwd, 12  *sizeof(char));
   ret = mpq_standby_uart_write(&checksum, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x10)
   {
      MPQ_SP_DBG_MSG("Failed to Set Password : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   /* Send Base Addr. + Image Size */
   swapped_base_addr  = LE2BE_LONG(base_addr);
   ret = mpq_standby_uart_write((unsigned char *)&swapped_base_addr,
                                sizeof(unsigned int));

   swapped_image_size = LE2BE_SHORT(image_size);
   ret = mpq_standby_uart_write((unsigned char *)&swapped_image_size,
                                sizeof(unsigned short));

   memcpy(&passwd[0], &swapped_base_addr, 4);
   memcpy(&passwd[4], &swapped_image_size, 2);
   checksum = mpq_standby_calc_checksum(passwd, 6*sizeof(unsigned char));
   ret = mpq_standby_uart_write(&checksum, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x10)
   {
      MPQ_SP_DBG_MSG("Failed in Base Addr + Image Size : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   /* Send DLOAD Image */
   checksum = mpq_standby_calc_checksum(pDloadBuffer, image_size*sizeof(char));

   ret = mpq_standby_uart_write(pDloadBuffer, image_size*sizeof(char));

   ret = mpq_standby_uart_write(&checksum, sizeof(char));

   ack = 0x00;
   ret = mpq_standby_uart_read(&ack, sizeof(char));

   if (ack != 0x10)
   {
      MPQ_SP_DBG_MSG("Failed in Image Download : ACK 0x%02x\n", ack);
      ret =  -EIO;
      goto cleanup;
   }

   /* Transfer Actual Image */
   while (loop_here)
   {
      /* Wait for the SP to be Ready */
      ack = 0;
      ret = mpq_standby_uart_read(&ack, sizeof(char));

      if (ack != 'S')
      {
         MPQ_SP_DBG_MSG("Failed to get response from MCU for APP DLOAD\n");
         ret = -EIO;
         goto cleanup;
      }

      memset(pAppsBuffer, 0xFF, MPQ_DLOAD_BUFFER_SIZE);

      /* Read 512-Bytes from the File */
      bytes_ret = fread(pAppsBuffer, 1, MPQ_DLOAD_BUFFER_SIZE, pAppsImage);
      if (bytes_ret != MPQ_DLOAD_BUFFER_SIZE)
      {
          MPQ_SP_DBG_MSG("fread only %d bytes out of %d bytes\n", bytes_ret,
                         MPQ_DLOAD_BUFFER_SIZE);
          loop_here = FALSE;
      }

      /* Write to SP */
      ret = mpq_standby_uart_write(pAppsBuffer, MPQ_DLOAD_BUFFER_SIZE);
      if (ret)
      {
         MPQ_SP_DBG_MSG("UART write returned %d bytes instead of %d\n",
                        ret, MPQ_DLOAD_BUFFER_SIZE);
         ret = -EIO;
         goto cleanup;
      }
   }

   /* Send Terminate String */
   memset(pAppsBuffer, 0xFF, MPQ_DLOAD_BUFFER_SIZE);
   memcpy(pAppsBuffer, term_str, 10);

   ret = mpq_standby_uart_write(pAppsBuffer, MPQ_DLOAD_BUFFER_SIZE);
   if (ret)
   {
      MPQ_SP_DBG_MSG("UART write returned %d bytes instead of %d\n",
                     ret, MPQ_DLOAD_BUFFER_SIZE);
      ret = -EIO;
      goto cleanup;
   }

   usleep(10000);
   MPQ_SP_DBG_MSG("Firmware Download Complete\n");

cleanup:
   if (pDloadBuffer)
      free(pDloadBuffer);

   if (pAppsBuffer)
      free(pAppsBuffer);

   mpq_standby_dload_term();
   mpq_stand_dload_image_term();
   return ret;
}


