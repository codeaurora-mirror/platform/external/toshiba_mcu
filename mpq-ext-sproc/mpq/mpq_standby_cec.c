/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <assert.h>
#include <pthread.h>
#include <linux/input.h>

#include <mpq_standby_utils.h>

typedef struct
{
    unsigned char Initiator;
    unsigned char Destination;
    unsigned char Opcode;
    unsigned char CEC_Data[17];
    unsigned char current_num;
    unsigned char Max_num;
    unsigned char current_state;
} MPQ_CEC_FrameTypeDef;

static int mpq_standby_hdmi_cec_msg_event(void *buffer)
{
   MPQ_CEC_FrameTypeDef *pCECFrame = (MPQ_CEC_FrameTypeDef *)buffer;
   int i = 0;

   MPQ_SP_DBG_MSG("\n---------------------------------------------\n");
   MPQ_SP_DBG_MSG("CEC Event Received\n");
   MPQ_SP_DBG_MSG("Source : %d\n", pCECFrame->Initiator);
   MPQ_SP_DBG_MSG("Destination : %d\n", pCECFrame->Destination);
   MPQ_SP_DBG_MSG("Opcode : %d\n", pCECFrame->Opcode);
   MPQ_SP_DBG_MSG("CEC Data : \n");
   for (i = 0; i < 17; i++)
   {
      MPQ_SP_DBG_MSG("0x%02x ", pCECFrame->CEC_Data[i]);
   }
   MPQ_SP_DBG_MSG("\n");
   MPQ_SP_DBG_MSG("Current Num: %d\n", pCECFrame->current_num);
   MPQ_SP_DBG_MSG("Max. Num : %d\n", pCECFrame->Max_num);
   MPQ_SP_DBG_MSG("Data State : %d\n", pCECFrame->current_state);
   MPQ_SP_DBG_MSG("\n---------------------------------------------\n");

   return 0;
}

int mpq_standby_hdmi_cec_handler(unsigned char *buffer)
{
   int ret = 0;

   if (NULL == buffer)
      return -EINVAL;

   switch (buffer[0])
   {
      case SP_HDMI_CEC_MSG_EVENT:
         ret = mpq_standby_hdmi_cec_msg_event((void *)&buffer[1]);
         break;
      default:
         break;
   }

   return ret;
}
