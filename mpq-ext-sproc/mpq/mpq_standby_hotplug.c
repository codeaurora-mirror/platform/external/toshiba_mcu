/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <assert.h>
#include <pthread.h>

#include <mpq_standby_utils.h>

static int mpq_standby_hdmi_hotplug_handler(void *buffer)
{
   unsigned char *pHdmiHotplugMask = (unsigned char *)buffer;

   MPQ_SP_DBG_MSG("HDMI Hot Plug Mask : 0x%02x \n", *pHdmiHotplugMask);

   return 0;
}

static int mpq_standby_vga_hotplug_handler(void *buffer)
{
   unsigned char *pVGAHotplugMask = (unsigned char *)buffer;

   MPQ_SP_DBG_MSG("VGA Hot Plug Mask : 0x%02x \n", *pVGAHotplugMask);

   return 0;
}

int mpq_standby_hotplug_handler(unsigned char *buffer)
{
   int ret = 0;

   if (NULL == buffer)
      return -EINVAL;

   switch (buffer[0])
   {
      case SP_HDMI_HOTPLUG_EVENT:
         ret = mpq_standby_hdmi_hotplug_handler((void *)&buffer[1]);
         break;
      case SP_VGA_HOTPLUG_EVENT:
         ret = mpq_standby_vga_hotplug_handler((void *)&buffer[1]);
         break;
      default:
         break;
   }

   return ret;
}


