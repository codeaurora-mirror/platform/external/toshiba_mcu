/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_fc.h
* @brief   This file provides all the functions prototypes for FC driver.
* @version V2.1.0
* @date    2010/07/20
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TMPM330_FC_H
#define __TMPM330_FC_H

#ifndef TMPM330FDFG
#define TMPM330FDFG

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "TMPM330.h"
#include "tx03_common.h"

#if defined(__TMPM330_FC_H) || defined(__TMPM332_FC_H) || defined(__TMPM333_FC_H)
/** @addtogroup TX03_Periph_Driver
  * @{
  */

/** @addtogroup FC
  * @{
  */

/** @addtogroup FC_Exported_types
  * @{
  */
    typedef enum {
        FC_SUCCESS = 0U,
        FC_ERROR_PROTECTED = 1U,
        FC_ERROR_OVER_TIME = 2U
    } FC_Result;

#if defined(__TMPM330_FC_H) || defined(__TMPM333_FC_H)
#if defined(TMPM330FDFG) || defined(TMPM333FDFG)
    typedef enum {
        FC_BLOCK_0 = 0,
        FC_BLOCK_1 = 1,
        FC_BLOCK_2 = 2,
        FC_BLOCK_3 = 3,
        FC_BLOCK_4 = 4,
        FC_BLOCK_5 = 5
    } FC_BlockNum;

#define IS_FC_BLOCK_NUM(param)  (((param) == FC_BLOCK_0)|| \
                                 ((param) == FC_BLOCK_1)|| \
                                 ((param) == FC_BLOCK_2)|| \
                                 ((param) == FC_BLOCK_3)|| \
                                 ((param) == FC_BLOCK_4)|| \
                                 ((param) == FC_BLOCK_5))

#endif                          /* (TMPM330FDFG) || (TMPM333FDFG) */
#elif defined(__TMPM332_FC_H)
#ifdef TMPM332FWUG
    typedef enum {
        FC_BLOCK_0 = 0,
        FC_BLOCK_1 = 1,
        FC_BLOCK_2 = 2,
        FC_BLOCK_3 = 3
    } FC_BlockNum;

#define IS_FC_BLOCK_NUM(param)  (((param) == FC_BLOCK_0)|| \
                                 ((param) == FC_BLOCK_1)|| \
                                 ((param) == FC_BLOCK_2)|| \
                                 ((param) == FC_BLOCK_3))
#endif                          /* (TMPM332FWUG) */
#endif                          /* (__TMPM330_FC_H) */
/** @} */
/* End of group FC_Exported_types */

/** @defgroup FC_Exported_Constants
  * @{
  */
#define FLASH_PAGE_SIZE                 ((uint32_t)0x00000200)  /* Page Size 512 Byte */

#define FC_BLOCK_GROUP_0                 ((uint8_t)0x00)
#define FC_BLOCK_GROUP_1                 ((uint8_t)0x01)
#define IS_FC_BLOCK_GROUP(param)        (((param) == FC_BLOCK_GROUP_0) || \
                                         ((param) == FC_BLOCK_GROUP_1))

#ifdef SINGLE_BOOT_MODE
#define FLASH_START_ADDR                 ((uint32_t)0x3F800000) /* SINGLE_BOOT_MODE */
#else
#define FLASH_START_ADDR                 ((uint32_t)0x00000000) /* User Boot Mode As Default */
#endif

#define FLASH_END_ADDR                       (FLASH_START_ADDR + FLASH_CHIP_SIZE - 1U)

#ifdef SINGLE_BOOT_MODE
#define IS_FC_ADDR(param)                (((param) >= FLASH_START_ADDR) && \
                                          ((param) <= FLASH_END_ADDR))
#else
#define IS_FC_ADDR(param)                ((param) <= FLASH_END_ADDR)
#endif

#define IS_FC_PAGE_ADDR(param)          ((((param) - FLASH_START_ADDR) % FLASH_PAGE_SIZE) == 0U)

/** @} */
/* End of group FC_Exported_Constants */

/** @defgroup FC_Exported_FunctionPrototypes
  * @{
  */

    void FC_SetSecurityBit(FunctionalState NewState);
    FunctionalState FC_GetSecurityBit(void);
    WorkState FC_GetBusyState(void);
    FunctionalState FC_GetBlockProtectState(FC_BlockNum BlockNum);
	FC_Result FC_ProgramBlockProtectState(FC_BlockNum BlockNum);
	FC_Result FC_EraseBlockProtectState(uint8_t BlockGroup);
	FC_Result FC_WritePage(uint32_t PageAddr, uint32_t * Data);
	FC_Result FC_EraseBlock(uint32_t BlockAddr);
	FC_Result FC_EraseChip(void);

/** @} */
/* End of group FC_Exported_FunctionPrototypes */

/** @} */
/* End of group FC */

/** @} */
/* End of group TX03_Periph_Driver */
#endif                          /*(__TMPM330_FC_H) || (__TMPM332_FC_H) || (__TMPM333_FC_H) */

#ifdef __cplusplus
}
#endif
#endif                          /* TMPM330FDFG */
#endif                          /* __TMPM330_FC_H */
