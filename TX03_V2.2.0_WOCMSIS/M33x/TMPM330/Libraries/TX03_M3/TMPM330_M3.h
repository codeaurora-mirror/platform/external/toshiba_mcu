/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* File Name          : TMPM330_M3.h
* Version            : V1.0
* Date               : 26 Sep 2012
* Description        : TMPM330_M3 Core Peripheral Access Layer Header File
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/** @addtogroup TOSHIBA_TX03_MICROCONTROLLER
  * @{
  */

/** @addtogroup TMPM330_M3
  * @{
  */

#ifndef __TMPM330_M3_H__
#define __TMPM330_M3_H__

 
/*  M3 definitions */
#define __ASM            __asm           /*!< asm keyword for ARM Compiler          */
#define __INLINE         __inline        /*!< inline keyword for ARM Compiler       */

#include "type_def.h"                    /*!< standard types definitions                      */
#define __WFI                             __wfi
#define __DSB()                           __dsb(0)
#define __NOP                             __nop

/* IO definitions (access restrictions to peripheral registers) */
#define   __I     volatile const       /*!< defines 'read only' permissions                 */
#define   __O     volatile             /*!< defines 'write only' permissions                */
#define   __IO    volatile             /*!< defines 'read / write' permissions              */




/*******************************************************************************
 *                 Register Abstraction
 ******************************************************************************/
/** \brief   memory mapped structure for System Control Block (SCB)
  */
typedef struct
{
  __I  uint32_t CPUID;                        /*!< Offset: 0x00  CPU ID Base Register                                  */
  __IO uint32_t ICSR;                         /*!< Offset: 0x04  Interrupt Control State Register                      */
  __IO uint32_t VTOR;                         /*!< Offset: 0x08  Vector Table Offset Register                          */
  __IO uint32_t AIRCR;                        /*!< Offset: 0x0C  Application Interrupt / Reset Control Register        */
  __IO uint32_t SCR;                          /*!< Offset: 0x10  System Control Register                               */
  __IO uint32_t CCR;                          /*!< Offset: 0x14  Configuration Control Register                        */
  __IO uint8_t  SHP[12];                      /*!< Offset: 0x18  System Handlers Priority Registers (4-7, 8-11, 12-15) */
  __IO uint32_t SHCSR;                        /*!< Offset: 0x24  System Handler Control and State Register             */
  __IO uint32_t CFSR;                         /*!< Offset: 0x28  Configurable Fault Status Register                    */
  __IO uint32_t HFSR;                         /*!< Offset: 0x2C  Hard Fault Status Register                            */
  __IO uint32_t DFSR;                         /*!< Offset: 0x30  Debug Fault Status Register                           */
  __IO uint32_t MMFAR;                        /*!< Offset: 0x34  Mem Manage Address Register                           */
  __IO uint32_t BFAR;                         /*!< Offset: 0x38  Bus Fault Address Register                            */
  __IO uint32_t AFSR;                         /*!< Offset: 0x3C  Auxiliary Fault Status Register                       */
  __I  uint32_t PFR[2];                       /*!< Offset: 0x40  Processor Feature Register                            */
  __I  uint32_t DFR;                          /*!< Offset: 0x48  Debug Feature Register                                */
  __I  uint32_t ADR;                          /*!< Offset: 0x4C  Auxiliary Feature Register                            */
  __I  uint32_t MMFR[4];                      /*!< Offset: 0x50  Memory Model Feature Register                         */
  __I  uint32_t ISAR[5];                      /*!< Offset: 0x60  ISA Feature Register                                  */
} SCB_Type;                                                

/** \brief  Structure type to access the Nested Vectored Interrupt Controller (NVIC).
 */
typedef struct
{
  __IO uint32_t ISER[8];                 /*!< Offset: 0x000 (R/W)  Interrupt Set Enable Register           */
       uint32_t RESERVED0[24];
  __IO uint32_t ICER[8];                 /*!< Offset: 0x080 (R/W)  Interrupt Clear Enable Register         */
       uint32_t RSERVED1[24];
  __IO uint32_t ISPR[8];                 /*!< Offset: 0x100 (R/W)  Interrupt Set Pending Register          */
       uint32_t RESERVED2[24];
  __IO uint32_t ICPR[8];                 /*!< Offset: 0x180 (R/W)  Interrupt Clear Pending Register        */
       uint32_t RESERVED3[24];
  __IO uint32_t IABR[8];                 /*!< Offset: 0x200 (R/W)  Interrupt Active bit Register           */
       uint32_t RESERVED4[56];
  __IO uint8_t  IP[240];                 /*!< Offset: 0x300 (R/W)  Interrupt Priority Register (8Bit wide) */
       uint32_t RESERVED5[644];
  __O  uint32_t STIR;                    /*!< Offset: 0xE00 ( /W)  Software Trigger Interrupt Register     */
}  NVIC_Type;

/* Memory mapping of Cortex-M3 Hardware */
#define SCS_BASE            (0xE000E000UL)                            /*!< System Control Space Base Address  */
#define NVIC_BASE           (SCS_BASE +  0x0100UL)                    /*!< NVIC Base Address                  */
#define SCB_BASE            (SCS_BASE +  0x0D00)                      /*!< System Control Block Base Address */

#define NVIC                ((NVIC_Type      *)     NVIC_BASE     )   /*!< NVIC configuration struct          */
#define SCB                 ((SCB_Type *)           SCB_BASE)         /*!< SCB configuration struct          */

/* SCB Application Interrupt and Reset Control Register Definitions */
#define SCB_AIRCR_VECTKEY_Pos              16                                             /*!< SCB AIRCR: VECTKEY Position */
#define SCB_AIRCR_VECTKEY_Msk              (0xFFFFul << SCB_AIRCR_VECTKEY_Pos)            /*!< SCB AIRCR: VECTKEY Mask */
#define SCB_AIRCR_PRIGROUP_Pos              8                                             /*!< SCB AIRCR: PRIGROUP Position */
#define SCB_AIRCR_PRIGROUP_Msk             (7ul << SCB_AIRCR_PRIGROUP_Pos)                /*!< SCB AIRCR: PRIGROUP Mask */
#define SCB_AIRCR_SYSRESETREQ_Pos           2                                             /*!< SCB AIRCR: SYSRESETREQ Position */
#define SCB_AIRCR_SYSRESETREQ_Msk          (1ul << SCB_AIRCR_SYSRESETREQ_Pos)             /*!< SCB AIRCR: SYSRESETREQ Mask */



/*******************************************************************************
 *                Hardware Abstraction Layer
 ******************************************************************************/
/*  NVIC functions   */
/** \brief  Enable External Interrupt

    This function enables a device specific interrupt in the NVIC interrupt controller.

    \param [in]      intr_no  Number of the external interrupt to enable
 */
static __INLINE void NVIC_Enable_Interrupt(interrupt_type intr_no)
{
  NVIC->ISER[((uint32_t)(intr_no) >> 5)] = (1 << ((uint32_t)(intr_no) & 0x1F)); /* enable interrupt */
}


/** \brief  Disable External Interrupt

    This function disables a device specific interrupt in the NVIC interrupt controller.

    \param [in]      intr_no  Number of the external interrupt to disable
 */
static __INLINE void NVIC_Disable_Interrupt(interrupt_type intr_no)
{
  NVIC->ICER[((uint32_t)(intr_no) >> 5)] = (1 << ((uint32_t)(intr_no) & 0x1F)); /* disable interrupt */
}


/** \brief  Clear the pending bit for an external interrupt
 *
 * \param [in] Interruptn    The number of the interrupt for clear pending
 *
 * Clear the pending bit for the specified interrupt. 
 * The interrupt number cannot be a negative value.
 */
static __INLINE void NVIC_ClearPending_Interrupt(interrupt_type Interruptn)
{
  NVIC->ICPR[((uint32_t)(Interruptn) >> 5)] = (1 << ((uint32_t)(Interruptn) & 0x1F)); /* Clear pending interrupt */
}

/*@}*/ /* end of group TMPM330_M3_FunctionInterface */


#endif  /* __TMPM330_M3_H__ */

/** @} */ /* End of group TMPM330_M3 */
/** @} */ /* End of group TOSHIBA_TX03_MICROCONTROLLER */
