/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   The demo functions of CG for the TOSHIBA 'TMPM330' Device Series 
* @version V1.200
* @date    2010/06/22
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

#include "tmpm330_cg.h"
#include "lcd.h"

typedef enum { KEY1 = 1, KEY2, KEY3, KEY4 } KEYID;

#define MODE_NORMAL         ((uint8_t)0x00)
#define MODE_TOBESLEEP      ((uint8_t)0x01)
#define MODE_INSLEEP        ((uint8_t)0x02)
#define MODE_TOBENORMAL     ((uint8_t)0x03)
void Key_Configuration(void);
void Key_WaitRelease(KEYID key);
Result CG_EnableClkMulCircuit(void);
uint8_t gStandByMode = MODE_NORMAL;
int main(void)
{
    /*SystemInit(); */
    /* Set CG Module  */
    /* set fgear = fc/2 */
    CG_SetFgearLevel(CG_DIVIDE_2);
    /* set fperi = fgear  fpreclk = fperi/32 */
    CG_SetPhiT0Level(CG_DIVIDE_64);
    /* set SCOUT source to fT0 */
    CG_SetSCOUTSrc(CG_SCOUT_SRC_PHIT0);
    /* enable high-speed oscillation */
    CG_SetFosc(ENABLE);
    /* enable low-speed oscillation */
    CG_SetFs(ENABLE);
    /* set low power consumption mode sleep */
    CG_SetSTBYMode(CG_STBY_MODE_SLEEP);
    /* set high-speed and low-speed oscillation to be enabled after releasing stop mode */
    CG_SetExitStopModeFosc(ENABLE);
    CG_SetExitStopModeFs(ENABLE);
    /* set pin status in stop mode to "active" */
    CG_SetPinStateInStopMode(ENABLE);
    /*set up pll and wait 200us for pll to warm up , set fc source to fpll */
    CG_EnableClkMulCircuit();
    /* set system clock to fgear */
    CG_SetFsysSrc(CG_FSYS_SRC_FGEAR);

    Key_Configuration();
    LCD_Configuration();
    LCD_Light(ON);

    do {
        Send_To_LCD(FIRST_LINE, LCD_COMMAND);
        Send_LCD_Text("Nrml->Slow->Slp");
        Send_To_LCD(SECOND_LINE, LCD_COMMAND);
        Send_LCD_Text("Press K1: Start");
        Key_WaitRelease(KEY1);
        gStandByMode = MODE_TOBESLEEP;

        if (MODE_TOBESLEEP == gStandByMode) {
            Send_To_LCD(FIRST_LINE, LCD_COMMAND);
            Send_LCD_Text("");
            Send_To_LCD(SECOND_LINE, LCD_COMMAND);
            Send_LCD_Text("");
            /* set CG module: Normal ->Slow mode */
            /*... add code here to disable modules (except CPU,RTC,I/O,CEC and RMC) if it has been enabled... */
            /* then set system clock to fs */
            CG_SetFsysSrc(CG_FSYS_SRC_FS);
            /* then stop high-speed oscillation */
            CG_SetFosc(DISABLE);

            Send_To_LCD(FIRST_LINE, LCD_COMMAND);
            Send_LCD_Text("in slow mode");

            /* Set CG Module : Slow ->Sleep mode */
            __disable_irq();
            /* set source(RTC interrupt) to exit sleep mode */
            CG_SetSTBYReleaseINTSrc(CG_INT_SRC_RTC, CG_INT_ACTIVE_STATE_FALLING, ENABLE);
            /* enable RTC interrupt */
            NVIC_ClearPending_Interrupt(INTRTC_interrupt);
            NVIC_Enable_Interrupt(INTRTC_interrupt);
            /*clear interrupt request */
            CG_ClearINTReq(CG_INT_SRC_RTC);
            /* set current time */
            TSB_RTC->RESTR = 0xf0U;
            TSB_RTC->PAGER = 0x00U;
            TSB_RTC->SECR = 0x00U;
            TSB_RTC->MINR = 0x00U;
            TSB_RTC->HOURR = 0x00U;
            TSB_RTC->DAYR = 0x01U;
            TSB_RTC->DATER = 0x01U;
            TSB_RTC->MONTHR = 0x01U;
            TSB_RTC->YEARR = 0x01U;

            /* alarm disable, page =1 */
            TSB_RTC->PAGER = 0x1U;
            /* set alarm time = clock+2min */
            TSB_RTC->MINR = 0x02U;
            TSB_RTC->HOURR = 0x00U;
            TSB_RTC->DAYR = 0x01U;
            TSB_RTC->DATER = 0x01U;
            TSB_RTC->MONTHR = 0x01U;
            /* enable alarm */
            TSB_RTC->PAGER = 0x0cU;
            /* enable RTC interrupt */
            TSB_RTC->PAGER = 0x8cU;
            __enable_irq();

            Send_To_LCD(FIRST_LINE, LCD_COMMAND);
            Send_LCD_Text("to be sleep mode");
            LCD_Light(OFF);
            gStandByMode = MODE_INSLEEP;
            /* enter sleep mode */
            __WFI();
            /* Interrupt is needed to exit sleep mode */
            /* wait 2 min to generate RTC interrupt */
            LCD_Light(ON);
        }

        if (MODE_TOBENORMAL == gStandByMode) {
            Send_To_LCD(FIRST_LINE, LCD_COMMAND);
            Send_LCD_Text("wk up, slow mode");
            /* set CG module :Slow -> Normal */
            /* start high-speed oscillation */
            CG_SetFosc(ENABLE);
            /* wait 1.953ms for fosc to warm up */
            CG_SetWarmUpTime(CG_WARM_UP_SRC_XT1, CG_WARM_UP_TIME_EXP_6);
            /* start warm up */
            CG_StartWarmUp();
            /* check whether warm up ends or not */
            /* check whether warm up ends or not */
            while (CG_GetWarmUpState() == BUSY);
            /* set system clock to clock-gear */
            CG_SetFsysSrc(CG_FSYS_SRC_FGEAR);
            gStandByMode = MODE_NORMAL;
        }
    } while (1);
}

void Key_Configuration(void)
{
    TSB_PB->IE |= (uint32_t) 0x00F0;
    TSB_PB->PUP |= 0x00F0;
}

void Key_WaitRelease(KEYID key)
{
    switch (key) {
    case KEY1:
        while (TSB_PB_DATA_PB7);
        while (!TSB_PB_DATA_PB7);
        break;
    case KEY2:
        while (TSB_PB_DATA_PB6);
        while (!TSB_PB_DATA_PB6);
        break;
    case KEY3:
        while (TSB_PB_DATA_PB5);
        while (!TSB_PB_DATA_PB5);
        break;
    case KEY4:
        while (TSB_PB_DATA_PB4);
        while (!TSB_PB_DATA_PB4);
        break;
    default:
        break;
    }
}

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif

void INTRTC_IRQHandler()
{
    gStandByMode = MODE_TOBENORMAL;
    CG_ClearINTReq(CG_INT_SRC_RTC);
}


Result CG_EnableClkMulCircuit(void)
{
    Result retval = ERROR;
    WorkState st = BUSY;
    retval = CG_SetPLL(ENABLE);    
    if (retval == SUCCESS) {
        /*set warm up time to about 200us */
        retval = CG_SetWarmUpTime(CG_WARM_UP_SRC_X1, CG_WARM_UP_TIME_EXP_11);
        if (retval == SUCCESS) {
            CG_StartWarmUp();
            /*wait warm up to end */
            do {
                st = CG_GetWarmUpState();
            } while (st != DONE);
            retval = CG_SetFcSrc(CG_FC_SRC_FPLL);
        } else {
            /*Do nothing */
        }
    } else {
        /*Do nothing */
    }
    return retval;
}
