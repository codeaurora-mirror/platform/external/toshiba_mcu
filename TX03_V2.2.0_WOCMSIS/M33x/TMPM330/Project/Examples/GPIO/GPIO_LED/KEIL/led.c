/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    led.c
* @brief   LED driver for the TOSHIBA 'TMPM330' Device Series
* @version V0.200
* @date    2010/06/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "led.h"

/*******************************************************************************
* Function Name  : LED_Display
* Description    : Turn on or off LED
* Input          : ucLED
* Return         : None.
*******************************************************************************/
void LED_Display(uint8_t ucLED)
{
    uint8_t tmp = 0U;
    tmp = ucLED & 0x0f;
    GPIO_WriteData(GPIO_PG, tmp);
    tmp = ((uint8_t) (ucLED >> 4) & 0x0f);
    GPIO_WriteData(GPIO_PJ, tmp);
}


/*******************************************************************************
* Function Name  : LED_Init
* Description    : Configure the GPIO to LED
* Input          : None.
* Return         : None.
*******************************************************************************/
void LED_Init(void)
{
    GPIO_InitTypeDef led_io;
    led_io.IOMode = GPIO_OUTPUT_MODE;
    led_io.PullUp = GPIO_PULLUP_ENABLE;
    led_io.PullDown = GPIO_PULLDOWN_NONE;
    led_io.OpenDrain = GPIO_OPEN_DRAIN_NONE;
    GPIO_Init(GPIO_PG, GPIO_BIT_0, &led_io);
    GPIO_Init(GPIO_PG, GPIO_BIT_1, &led_io);
    GPIO_Init(GPIO_PG, GPIO_BIT_2, &led_io);
    GPIO_Init(GPIO_PG, GPIO_BIT_3, &led_io);
    GPIO_Init(GPIO_PJ, GPIO_BIT_0, &led_io);
    GPIO_Init(GPIO_PJ, GPIO_BIT_1, &led_io);
    GPIO_Init(GPIO_PJ, GPIO_BIT_2, &led_io);
    GPIO_Init(GPIO_PJ, GPIO_BIT_3, &led_io);

    LED_Display(LEDALL_OFF);
}

/*******************************************************************************
* Function Name  : LED_On
* Description    : Turn on LED
* Input          : ucLED
* Return         : None.
*******************************************************************************/
void LED_On(uint8_t ucLED)
{
    if (ucLED & LED1) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_0, GPIO_BIT_VALUE_1);
    }
    if (ucLED & LED2) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_1, GPIO_BIT_VALUE_1);
    }
    if (ucLED & LED3) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_2, GPIO_BIT_VALUE_1);
    }
    if (ucLED & LED4) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_3, GPIO_BIT_VALUE_1);
    }
    if (ucLED & LED5) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_0, GPIO_BIT_VALUE_1);
    }
    if (ucLED & LED6) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_1, GPIO_BIT_VALUE_1);
    }
    if (ucLED & LED7) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_2, GPIO_BIT_VALUE_1);
    }
    if (ucLED & LED8) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_3, GPIO_BIT_VALUE_1);
    }
}

/*******************************************************************************
* Function Name  : LED_Off
* Description    : Turn off LED
* Input          : ucLED
* Return         : None.
*******************************************************************************/
void LED_Off(uint8_t ucLED)
{
    if (ucLED & LED1) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_0, GPIO_BIT_VALUE_0);
    }
    if (ucLED & LED2) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_1, GPIO_BIT_VALUE_0);
    }
    if (ucLED & LED3) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_2, GPIO_BIT_VALUE_0);
    }
    if (ucLED & LED4) {
        GPIO_WriteDataBit(GPIO_PG, GPIO_BIT_3, GPIO_BIT_VALUE_0);
    }
    if (ucLED & LED5) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_0, GPIO_BIT_VALUE_0);
    }
    if (ucLED & LED6) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_1, GPIO_BIT_VALUE_0);
    }
    if (ucLED & LED7) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_2, GPIO_BIT_VALUE_0);
    }
    if (ucLED & LED8) {
        GPIO_WriteDataBit(GPIO_PJ, GPIO_BIT_3, GPIO_BIT_VALUE_0);
    }
}
