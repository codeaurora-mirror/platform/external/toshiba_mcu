/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   the application functions of GPIO demo for the
*          TOSHIBA 'TMPM330' Device Series
* @version V0.200
* @date    2010/06/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* include file */
#include "tmpm330_gpio.h"
#include "key.h"
#include "led.h"

/*  Function declaration */
void demo(void);
void Delay(void);

/* main function */
int main(void)
{
    SystemInit();

    LED_Init();
    KEY_Init();
    while (1) {
        demo();
    }
}

void demo(void)
{
    if (KEY_Get(KEY1)) {
        LED_On(LED1);
    } else {
        LED_Off(LED1);
    }
    if (KEY_Get(KEY2)) {
        LED_On(LED2);
    } else {
        LED_Off(LED2);
    }
    if (KEY_Get(KEY3)) {
        LED_On(LED3);
    } else {
        LED_Off(LED3);
    }
    if (KEY_Get(KEY4)) {
        LED_On(LED4);
    } else {
        LED_Off(LED4);
    }
}

#ifdef DEBUG
void assert_failed(char* file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
