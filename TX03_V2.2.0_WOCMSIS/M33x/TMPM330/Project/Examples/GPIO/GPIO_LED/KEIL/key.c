/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    key.c
* @brief   Key driver for the TOSHIBA 'TMPM330' Device Series
* @version V0.200
* @date    2010/06/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "key.h"

/*******************************************************************************
* Function Name  : KEY_Init
* Description    : Configure the GPIO to KEY 
* Input          : None 
* Return         : None.
*******************************************************************************/
void KEY_Init(void)
{
    GPIO_SetInput(GPIO_PB, GPIO_BIT_4);
    GPIO_SetInput(GPIO_PB, GPIO_BIT_5);
    GPIO_SetInput(GPIO_PB, GPIO_BIT_6);
    GPIO_SetInput(GPIO_PB, GPIO_BIT_7);

    GPIO_SetPullUp(GPIO_PB, GPIO_BIT_4, ENABLE);
    GPIO_SetPullUp(GPIO_PB, GPIO_BIT_5, ENABLE);
    GPIO_SetPullUp(GPIO_PB, GPIO_BIT_6, ENABLE);
    GPIO_SetPullUp(GPIO_PB, GPIO_BIT_7, ENABLE);
}

/*******************************************************************************
* Function Name  : KEY_Get
* Description    : Get KEY Value
* Input          : ucKEY.
* Return         : KEY value.
*******************************************************************************/
uint8_t KEY_Get(uint8_t ucKEY)
{
    uint8_t ucVal = 0U;
    if (ucKEY & KEY1) {
        if (!GPIO_ReadDataBit(GPIO_PB, GPIO_BIT_7)) {
            ucVal |= 0x01;
        }
    }
    if (ucKEY & KEY2) {
        if (!GPIO_ReadDataBit(GPIO_PB, GPIO_BIT_6)) {
            ucVal |= 0x02;
        }
    }
    if (ucKEY & KEY3) {
        if (!GPIO_ReadDataBit(GPIO_PB, GPIO_BIT_5)) {
            ucVal |= 0x04;
        }
    }
    if (ucKEY & KEY4) {
        if (!GPIO_ReadDataBit(GPIO_PB, GPIO_BIT_4)) {
            ucVal |= 0x08;
        }
    }
    return (ucVal);
}
