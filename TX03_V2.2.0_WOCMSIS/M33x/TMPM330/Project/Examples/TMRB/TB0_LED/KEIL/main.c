/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   the application functions of TB0_LED demo for the TOSHIBA
*          'TMPM330' Device Series 
* @version V1.200
* @date    2010/06/09
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

#include "tmpm330_tmrb.h"
#include "tmpm330_gpio.h"
#include "led.h"
#define TMRB_1ms 0x1388U

/** 
 *  \brief
 *   This example generates the square wave output of 1KHz using TMRB Channel-0 
 *   PPG mode operation on port PI0. 
 */
int main(void)
{
    TMRB_InitTypeDef myTMRB;
	TMRB_FFOutputTypeDef ffopTMRB;


    myTMRB.Mode = TMRB_INTERVAL_TIMER;
    myTMRB.ClkDiv = TMRB_CLK_DIV_8;
    myTMRB.Cycle = TMRB_1ms;    /* Specific value depends on system clock */
    myTMRB.UpCntCtrl = TMRB_AUTO_CLEAR;
    myTMRB.Duty = TMRB_1ms / 2U;        /* Specific value depends on system clock */

    TMRB_Enable(TSB_TB0);
	TMRB_SetRunState(TSB_TB0, TMRB_STOP);
	TMRB_SetDoubleBuf(TSB_TB0, DISABLE);
    TMRB_Init(TSB_TB0, &myTMRB);
	TMRB_SetDoubleBuf(TSB_TB0, ENABLE);

	ffopTMRB.FlipflopCtrl = TMRB_FLIPFLOP_CLEAR;
	ffopTMRB.FlipflopReverseTrg = (TMRB_FLIPFLOP_MATCH_CYCLE | TMRB_FLIPFLOP_MATCH_DUTY);
	TMRB_SetFlipFlop(TSB_TB0, &ffopTMRB);
	TMRB_SetCaptureTiming(TSB_TB0, TMRB_DISABLE_CAPTURE);

	GPIO_EnableFuncReg(GPIO_PI, GPIO_FUNC_REG_1, GPIO_BIT_0);
	GPIO_SetOutputEnableReg(GPIO_PI, GPIO_BIT_0, ENABLE);

    TMRB_SetRunState(TSB_TB0, TMRB_RUN);
    NVIC_Enable_Interrupt(INTTB0_interrupt);
    while (1) {
        /* Do nothing */
    }
}

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
