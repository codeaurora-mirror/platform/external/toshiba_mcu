/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_sbi_int.c
* @brief   all interrupt request functions definition of Read EEPROM demo
*          for the TOSHIBA 'TMPM333' Device Series 
* @version V1.200
* @date    2010/6/23
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "tmpm330_sbi_int.h"

void INTSBI2_IRQHandler(void)
{
    uint32_t tmp = 0U;
    TSB_SBI_TypeDef *SBIx;
    SBI_I2CState sbi_sr;
    
    SBIx = TSB_SBI2;
    sbi_sr = SBI_GetI2CState(SBIx);

    if (sbi_sr.Bit.MasterSlave) {       /* Master mode     */
        if (sbi_sr.Bit.TRx) {   /* Tx mode        */
            if (sbi_sr.Bit.LastRxBit) {       /* LRB=1: the receiver requires no further data. */
                SBI_GenerateI2CStop(SBIx);
            } else {            /* LRB=0: the receiver requires further data. */
                if (gI2C_WCnt < gI2C_TxDataLen) {
                    SBI_SetSendData(SBIx, gI2C_TxData[gI2C_WCnt]);      /* Send next data */
                } else if (gI2C_WCnt == gI2C_TxDataLen) {       /* I2C data send finished. */
                    SBI_GenerateI2CStop(SBIx);
                } else {
                    /* Do nothing */
                }
                gI2C_WCnt++;
            }
        } else {                /* Rx Mode */

            if (gI2C_RCnt > gI2C_RxDataLen) {
                SBI_GenerateI2CStop(SBIx);
            } else {
                if (gI2C_RCnt == gI2C_RxDataLen) {      /* Rx last data */
                    SBI_SetI2CBitNum(SBIx, SBI_I2C_DATA_LEN_1);
                } else if (gI2C_RCnt == (gI2C_RxDataLen - 1U)) {        /* Rx the data second to last */
                    /* Not generate ACK for next data Rx end. */
                    SBI_SetI2CACK(SBIx, DISABLE);
                } else {
                    /* Do nothing */
                }
                tmp = SBI_GetReceiveData(SBIx);
                if (gI2C_RCnt > 0U) {
                    gI2C_RxData[gI2C_RCnt - 1U] = tmp;
                } else {
                    /* first read is dummy read */
                }
                gI2C_RCnt++;
            }
        }
    } else {                    /* Slave mode  */

    }

}
