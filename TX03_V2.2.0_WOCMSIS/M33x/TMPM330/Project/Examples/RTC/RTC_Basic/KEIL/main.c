/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   The application functions of RTC demo for the
*          TOSHIBA 'TMPM330' Device Series 
* @version V1.300
* @date    2010/07/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* include file */
#include "tmpm330_rtc.h"
#include "lcd.h"
#include "tmpm330_rtc_int.h"

#define CG_IMCGC_RTC_EDGE		((uint32_t)0x00000020)	/* RTC Interrupt Edge */
#define CG_IMCGC_RTC_INTEN		((uint32_t)0x00000021)	/* RTC Interrupt ON   */

/*  Function declaration */
void demo0(void);
void demo1(void);


/* User ram definition  */
RTC_DateTypeDef Date_Struct;
RTC_TimeTypeDef Time_Struct;

char RTC_Disp_YMD[16];
char RTC_Disp_HMS[16];
uint8_t Year;
uint8_t Month;
uint8_t Date;
uint8_t Day;
uint8_t Hour;
uint8_t Min;
uint8_t Sec;

/* main function */
int main(void)
{
    SystemInit();
    LCD_Configuration();
    LCD_Light(ON);

    __disable_irq();
    /* enable RTC interrupt */
    NVIC_Enable_Interrupt(INTRTC_interrupt);
    TSB_CG->IMCGC = CG_IMCGC_RTC_EDGE;
    TSB_CG->IMCGC = CG_IMCGC_RTC_INTEN;
    NVIC_Enable_Interrupt(INTRTC_interrupt);

    Date_Struct.LeapYear = RTC_LEAP_YEAR_2;
    Date_Struct.Year = (uint8_t) 10;
    Date_Struct.Month = (uint8_t) 12;
    Date_Struct.Date = (uint8_t) 31;
    Date_Struct.Day = RTC_FRI;

    Time_Struct.HourMode = RTC_12_HOUR_MODE;
    Time_Struct.Hour = (uint8_t) 11;
    Time_Struct.AmPm = RTC_PM_MODE;
    Time_Struct.Min = (uint8_t) 59;
    Time_Struct.Sec = (uint8_t) 50;

    RTC_DisableClock();
    RTC_DisableAlarm();

    /* Reset RTC sec counter */
    RTC_ResetClockSec();
    /* Set RTC Time value */
    RTC_SetTimeValue(&Time_Struct);
    /* Set RTC Date value */
    RTC_SetDateValue(&Date_Struct);

    /* Enable 1Hz interrupt */
    RTC_SetAlarmOutput(RTC_PULSE_1_HZ);
    /* Enable RTCINT */
    RTC_SetRTCINT(ENABLE);
    __enable_irq();

    /* waiting for RTC register set finish */
    while (fRTC_1HZ_INT != 1U) {
        /* Do nothing */
    }
    fRTC_1HZ_INT = 0U;

    /* Enable RTC Clock function */
    RTC_EnableClock();

    while (1) {
        if (fRTC_1HZ_INT == 1U) {
            fRTC_1HZ_INT = 0U;
            demo0();
            demo1();
        } else {
            /* Do nothing */
        }
    }
}


/* Send RTC Date value to LCD */
void demo0(void)
{
    /* Get RTC Date value */
    Year = RTC_GetYear();
    Month = RTC_GetMonth();
    Date = RTC_GetDate(RTC_CLOCK_MODE);
    Day = RTC_GetDay(RTC_CLOCK_MODE);
    /* Set LCD display */
    /* Dispaly year */
    RTC_Disp_YMD[0] = ' ';
    RTC_Disp_YMD[1] = (Year / 10U) + 0x30U;
    RTC_Disp_YMD[2] = (Year % 10U) + 0x30U;
    RTC_Disp_YMD[3] = '-';
    /* Display month */
    RTC_Disp_YMD[4] = (Month / 10U) + 0x30U;
    RTC_Disp_YMD[5] = (Month % 10U) + 0x30U;
    RTC_Disp_YMD[6] = '-';
    /* Display date */
    RTC_Disp_YMD[7] = (Date / 10U) + 0x30U;
    RTC_Disp_YMD[8] = (Date % 10U) + 0x30U;

    RTC_Disp_YMD[9] = ' ';
    RTC_Disp_YMD[10] = ' ';
    RTC_Disp_YMD[11] = ' ';
    /* Display day */
    switch (Day) {
    case RTC_SUN:
        RTC_Disp_YMD[12] = 'S';
        RTC_Disp_YMD[13] = 'U';
        RTC_Disp_YMD[14] = 'N';
        break;
    case RTC_MON:
        RTC_Disp_YMD[12] = 'M';
        RTC_Disp_YMD[13] = 'O';
        RTC_Disp_YMD[14] = 'N';
        break;
    case RTC_TUE:
        RTC_Disp_YMD[12] = 'T';
        RTC_Disp_YMD[13] = 'U';
        RTC_Disp_YMD[14] = 'E';
        break;
    case RTC_WED:
        RTC_Disp_YMD[12] = 'W';
        RTC_Disp_YMD[13] = 'E';
        RTC_Disp_YMD[14] = 'D';
        break;
    case RTC_THU:
        RTC_Disp_YMD[12] = 'T';
        RTC_Disp_YMD[13] = 'H';
        RTC_Disp_YMD[14] = 'U';
        break;
    case RTC_FRI:
        RTC_Disp_YMD[12] = 'F';
        RTC_Disp_YMD[13] = 'R';
        RTC_Disp_YMD[14] = 'I';
        break;
    case RTC_SAT:
        RTC_Disp_YMD[12] = 'S';
        RTC_Disp_YMD[13] = 'A';
        RTC_Disp_YMD[14] = 'T';
        break;
    default:
        /* Do nothing */
        break;
    }
    RTC_Disp_YMD[15] = ' ';
    Send_To_LCD(FIRST_LINE, LCD_COMMAND);
    Send_LCD_Text(RTC_Disp_YMD);
}

/* Send RTC Time value to LCD */
void demo1(void)
{
    RTC_GetTimeValue(&Time_Struct);
    /* LCD Display */
    RTC_Disp_HMS[0] = ' ';
    if (Time_Struct.AmPm == RTC_PM_MODE) {
        RTC_Disp_HMS[1] = 'P';
        RTC_Disp_HMS[2] = 'M';
    } else {
        RTC_Disp_HMS[1] = 'A';
        RTC_Disp_HMS[2] = 'M';
    }
    RTC_Disp_HMS[3] = ' ';
    /* Display hour */
    RTC_Disp_HMS[4] = (Time_Struct.Hour / 10U) + 0x30U;
    RTC_Disp_HMS[5] = (Time_Struct.Hour % 10U) + 0x30U;
    RTC_Disp_HMS[6] = ':';
    /* Display min */
    RTC_Disp_HMS[7] = (Time_Struct.Min / 10U) + 0x30U;
    RTC_Disp_HMS[8] = (Time_Struct.Min % 10U) + 0x30U;
    RTC_Disp_HMS[9] = ':';
    /* Display sec */
    RTC_Disp_HMS[10] = (Time_Struct.Sec / 10U) + 0x30U;
    RTC_Disp_HMS[11] = (Time_Struct.Sec % 10U) + 0x30U;
    RTC_Disp_HMS[12] = ' ';
    RTC_Disp_HMS[13] = ' ';
    RTC_Disp_HMS[14] = ' ';
    RTC_Disp_HMS[15] = ' ';
    Send_To_LCD(SECOND_LINE, LCD_COMMAND);
    Send_LCD_Text(RTC_Disp_HMS);
}

#ifdef DEBUG
void assert_failed(char* file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
