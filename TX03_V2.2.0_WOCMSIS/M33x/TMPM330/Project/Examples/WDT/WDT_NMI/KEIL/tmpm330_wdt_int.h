/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_wdt_int.h
* @brief   All interrupt request functions prototypes of WDT
*          for the TOSHIBA 'TMPM330' Device Series
* @version V1.100
* @date    2010/06/18
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TMPM330_WDT_INT_H
#define __TMPM330_WDT_INT_H

/* Includes ------------------------------------------------------------------*/
#include "TMPM330.h"
#include "tmpm330_wdt.h"
#include "led.h"


#define FLG_WDT    ((uint8_t)0x00000001)
#define FLG_NMI    ((uint8_t)0x00000002)

void NMI_Handler(void);

#endif                          /* __TMPM330_WDT_INT_H */
