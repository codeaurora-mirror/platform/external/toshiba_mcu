/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_wdt_int.c
* @brief   All interrupt request functions definition of WDT
*          for the TOSHIBA 'TMPM330' Device Series
* @version V1.100
* @date    2010/06/18
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "tmpm330_wdt_int.h"
uint8_t uCont = 0U;

/**
  * @brief  The NMI interrupt request function.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
    uint8_t uRegVal = 0U;
    uRegVal = ((uint8_t) TSB_CG->NMIFLG);
    switch (uRegVal) {
    case FLG_WDT:
        uCont++;
        LED_Toggle(LED1);
        if (uCont == 5U) {
            WDT_Disable();
            uCont = 0U;
        }
        break;
    case FLG_NMI:
        /* Do nothing */
        break;
    default:
        /* Do nothing */
        break;
    }
}
