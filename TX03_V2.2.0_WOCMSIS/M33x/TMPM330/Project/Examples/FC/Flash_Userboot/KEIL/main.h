/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.h
* @brief   header file for main.c, included some address definition and
*          function prototypes
* @version V0.100
* @date    2010/12/15
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

#ifndef MAIN_H
#define MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "tmpm330_cg.h"
#include "tmpm330_fc.h"
#include "TMPM330.h"
#include "tmpm330_gpio.h"

//#include "led.h"

/* Private define ------------------------------------------------------------*/
#define USER_BOOT_MODE          ((uint8_t)0x01)
#define NORMAL_MODE             ((uint8_t)0x00)

/* RealView Compiler */
#define CODE_START              ((void (*)(void))0x4000)
#define FLASH_API_ROM           ((uint32_t *)__Get_FLASHSWAP_Store_Base())

#define DEMO_A_FLASH            (0x00004000)

#define RAM_BUFFER_START        ((uint32_t)0x20000000)
#define FLASH_API_RAM           ((uint32_t *)(RAM_BUFFER_START))

#define SIZE_FLASH_API          (0x400U)



#define PAGE_SIZE               (512U)

/* Private function prototypes -----------------------------------------------*/
void Copy_Routine(uint32_t * dest, uint32_t * source, uint32_t size);
uint8_t Mode_Judgement(void);
uint8_t Write_Flash(uint32_t * addr_dest, uint32_t * addr_source, uint32_t len);

#endif
/*************************** END OF FILE **************************************/
