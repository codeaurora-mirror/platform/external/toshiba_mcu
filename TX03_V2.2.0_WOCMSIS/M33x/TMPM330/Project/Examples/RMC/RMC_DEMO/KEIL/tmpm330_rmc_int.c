/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_rmc_int.c
* @brief   all interrupt request functions prototypes of RMC
*          for the TOSHIBA 'TMPM330' Device Series
* @version V2.000
* @date    2010/06/17
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "tmpm330_rmc_int.h"

extern RMCRX fRMCRx;
extern uint32_t RMC_Command;
void INTRMCRX0_IRQHandler(void)
{
    RMC_INTFactor myRMC_INTFactor;
    RMC_RxDataTypeDef myRMC_RxDataDef;
#ifdef TOSHIBA_FORMAT_RMC
    RMC_LeaderDetection myRMC_LeaderDetection;
#endif

    myRMC_INTFactor = RMC_GetINTFactor(TSB_RMC0);
#ifdef TOSHIBA_FORMAT_RMC
    if (myRMC_INTFactor.Bit.MaxDataBitCycle) {  /*Max data bit cycle detection interrupt factor */
        myRMC_LeaderDetection = RMC_GetLeader(TSB_RMC0);
        if (myRMC_LeaderDetection == RMC_LEADER_DETECTED) {     /*Leader has been detected */
            myRMC_RxDataDef = RMC_GetRxData(TSB_RMC0);
            if (myRMC_RxDataDef.RxDataBits == 0x20U) {  /*32 bits data received */
                fRMCRx = Received;
                RMC_Command = myRMC_RxDataDef.RxBuf1;   /* save the RMC data for print in main() */
            }
        }
    }
#endif

#ifdef RC5_FORMAT_RMC
    if (myRMC_INTFactor.Bit.LowWidthDetection) {        /*Low width detection interrupt factor */
        myRMC_RxDataDef = RMC_GetRxData(TSB_RMC0);
        if (myRMC_RxDataDef.RxDataBits == 0x0EU) {      /*14 bits data received */
            fRMCRx = Received;
            RMC_Command = myRMC_RxDataDef.RxBuf1;       /* save the RMC data for print in main() */
        }
    }
#endif

    TSB_CG->ICRCG = 0x07U;       /* clear RMC0 INT */
}
