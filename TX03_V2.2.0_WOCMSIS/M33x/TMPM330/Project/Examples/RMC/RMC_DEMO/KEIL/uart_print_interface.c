/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    uart_print_interface.c
* @brief   the application functions to print data over UART0 for the TOSHIBA
*          'TMPM330' Device Series 
* @version V1.000
* @date    2012/10/17
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "uart_print_interface.h"
#include "tmpm330_uart.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t WrIndex = 0;
uint8_t RdIndex = 0;
char TxBuffer[BUFFER_SIZE] = {0};
uint8_t fUART0_INT = 0;
TrxState fUARTRxOK = NO;
char RxBuffer[8]={0};

/* Global variables ----------------------------------------------------------*/
/* external variables --------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name  : INTTX0_IRQHandler
* Description    : UART0 TX interrupt service routine
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void INTTX0_IRQHandler(void)
{
    if (RdIndex < WrIndex) {                              /* buffer is not empty */
        UART_SetTxData(UART0, (uint32_t) (TxBuffer[RdIndex++]));
        fUART0_INT = SET;                                        /* SIO0 int is enable */
    }
    else {
        fUART0_INT = CLEAR;                                      /* clear SIO0 INT flag */
        NVIC_Disable_Interrupt(INTTX0_interrupt);
    }
    
    if (RdIndex >= WrIndex) {                             /* reset buffer index */
        WrIndex = 0;
        RdIndex = 0;
    }
    else {
        /* do nothing */
    }
}

/*******************************************************************************
* Function Name  : INTRX0_IRQHandler
* Description    : UART0 RX interrupt service routine
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void INTRX0_IRQHandler(void)
{
    static uint8_t i = 0;

    fUARTRxOK = YES;                                     /* finish */
    i = 0;                                              /* reset count */
    RxBuffer[i++] = UART_GetRxData(UART0);
}

/*******************************************************************************
* Function Name  : UART_Init
* Description    : Initialize the uart
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void UART_Print_Init(void)
{
	UART_InitTypeDef myUART;

	/* UART0 Config */
    TSB_PE_CR_PE0C = 1U;
    TSB_PE_FR1_PE0F1 = 1U;
    TSB_PE_FR1_PE1F1 = 1U;
    TSB_PE_IE_PE1IE = 1U;

    myUART.BaudRate = 115200U;
    myUART.DataBits = UART_DATA_BITS_8;
    myUART.StopBits = UART_STOP_BITS_1;
    myUART.Parity = UART_NO_PARITY;
    myUART.Mode = UART_ENABLE_RX | UART_ENABLE_TX;
    myUART.FlowCtrl = UART_NONE_FLOW_CTRL;

    UART_Enable(UART0);
    UART_Init(UART0, &myUART);

    NVIC_Enable_Interrupt(INTRX0_interrupt);
}

/*******************************************************************************
* Function Name  : send_char
* Description    : Send character by SIO0
* Input          : Character to be sent
* Output         : None.
* Return         : Character to be sent
*******************************************************************************/
uint8_t send_char(uint8_t ch)
{
    TxBuffer[WrIndex++] = ch;                             
    if (fUART0_INT == CLEAR) {                            
        fUART0_INT = SET;                                 
        UART_SetTxData(UART0, (uint32_t) (TxBuffer[RdIndex++]));
	    NVIC_Enable_Interrupt(INTTX0_interrupt);
    }
    else {
        /* do nothing */
    }

    return ch;
}

/*******************************************************************************
* Function Name  : uart_print_str
* Description    : print string by SIO0
* Input          : string to print
* Output         : None.
* Return         : None
*******************************************************************************/
void uart_print_str(int8_t *str)
{
    if(0 == str)
        return;

    while(*str != 0)
    {
		send_char(*str);
        str++;
    }
}

/*******************************************************************************
* Function Name  : uart_print_byte
* Description    : print byte by SIO0
* Input          : bye to print
* Output         : None.
* Return         : None
*******************************************************************************/
void uart_print_byte(uint8_t byte_val)
{
    uint8_t hinib;
    uint8_t lonib;

    hinib = (uint8_t)((byte_val >> 4) & 0x0f);
    lonib = (byte_val & 0x0f);

    if(hinib <= 9) {
        hinib += '0';
        send_char(hinib);
    }
    else if((hinib >= 10) && (hinib <= 15)) {
        hinib += (uint8_t)('A' - 10);
        send_char(hinib);
    }

    if(lonib <= 9) {
        lonib += '0';
        send_char(lonib);
    }
    else if((lonib >= 10) && (lonib <= 15)) {
        lonib += (uint8_t)('A' - 10);
        send_char(lonib);
    }
}


/*******************************************************************************
* Function Name  : uart_print_short
* Description    : print short by SIO0
* Input          : bye to print
* Output         : None.
* Return         : None
*******************************************************************************/
void uart_print_short(uint16_t short_val)
{
    uint8_t hibyte;
    uint8_t lobyte;

    hibyte = (uint8_t)((short_val >> 8) & 0xff);
    lobyte = (uint8_t)(short_val & 0xff);

    uart_print_byte(hibyte);
    uart_print_byte(lobyte);
}


/*******************************************************************************
* Function Name  : uart_print_word
* Description    : print word by SIO0
* Input          : bye to print
* Output         : None.
* Return         : None
*******************************************************************************/
void uart_print_word(uint32_t word_val)
{
    uint8_t byte4, byte3;
    uint8_t byte2,byte1;

    byte4 = (uint8_t)((word_val >>24) & 0xff);
    byte3 = (uint8_t)((word_val >>16) & 0xff);
    byte2 = (uint8_t)((word_val >>8) & 0xff);
    byte1 = (uint8_t)((word_val) & 0xff);

    uart_print_byte(byte4);
    uart_print_byte(byte3);
    uart_print_byte(byte2);
    uart_print_byte(byte1);
}

/*******************************************************************************
* Function Name  : uart_get_char
* Description    : print short by SIO0
* Input          : bye to print
* Output         : None.
* Return         : None
*******************************************************************************/
int uart_get_char(void)
{
    fUARTRxOK = NO;
    return RxBuffer[0];
}

/*********************************** END OF FILE ******************************/
