/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   the application functions of RMC (Remote Controller) demo for the
*          TOSHIBA 'TMPM330' Device Series
* @version V2.000
* @date    2010/06/17
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
#include "main.h"
#include "uart_print_interface.h"

uint32_t RMC_Command;
RMCRX fRMCRx = None;

#define DEBUG

int main(void)
{
    TSB_RMC_TypeDef *RMCx;
    RMC_InitTypeDef myRMC;

    RMCx = TSB_RMC0;
    RMC_Command = 0U;
    SystemInit();

	UART_Print_Init();	

    RMC_Configuration(RMCx);    /*RMC IO initial */

    __disable_irq();
    TSB_CG->IMCGB = 0x30000000U;
    TSB_CG->IMCGB = 0x31000000U;
    TSB_CG->ICRCG = 0x07U;

#ifdef TOSHIBA_FORMAT_RMC
    /*RMC register set for TOSHIBA format */
    myRMC.LeaderPara.MaxCycle = RMC_MAX_CYCLE;
    myRMC.LeaderPara.MinCycle = RMC_MIN_CYCLE;
    myRMC.LeaderPara.MaxLowWidth = RMC_MAX_LOW_WIDTH;
    myRMC.LeaderPara.MinLowWidth = RMC_MIN_LOW_WIDTH;
    myRMC.LeaderPara.LeaderDetectionState = ENABLE;
    myRMC.LeaderPara.LeaderINTState = DISABLE;
    myRMC.FallingEdgeINTState = DISABLE;
    myRMC.SignalRxMethod = RMC_RX_IN_CYCLE_METHOD;
    myRMC.LowWidth = RMC_TRG_LOW_WIDTH;
    myRMC.MaxDataBitCycle = RMC_TRG_MAX_DATA_BIT_CYCLE;
    myRMC.LargerThreshold = RMC_LARGER_THRESHOLD;
    myRMC.SmallerThreshold = RMC_SMALLER_THRESHOLD;
    myRMC.InputSignalReversedState = DISABLE;
    myRMC.NoiseCancellationTime = RMC_NOISE_CANCELLATION_TIME;
#endif

#ifdef RC5_FORMAT_RMC
    /*RMC register set for RC5 format */
    myRMC.LeaderPara.MaxCycle = RMC_MAX_CYCLE;
    myRMC.LeaderPara.MinCycle = RMC_MIN_CYCLE;
    myRMC.LeaderPara.MaxLowWidth = RMC_MAX_LOW_WIDTH;
    myRMC.LeaderPara.MinLowWidth = RMC_MIN_LOW_WIDTH;
    myRMC.LeaderPara.LeaderDetectionState = DISABLE;
    myRMC.LeaderPara.LeaderINTState = DISABLE;
    myRMC.FallingEdgeINTState = DISABLE;
    myRMC.SignalRxMethod = RMC_RX_IN_PHASE_METHOD;
    myRMC.LowWidth = RMC_TRG_LOW_WIDTH;
    myRMC.MaxDataBitCycle = RMC_TRG_MAX_DATA_BIT_CYCLE;
    myRMC.LargerThreshold = RMC_LARGER_THRESHOLD;
    myRMC.SmallerThreshold = RMC_SMALLER_THRESHOLD;
    myRMC.InputSignalReversedState = ENABLE;
    myRMC.NoiseCancellationTime = RMC_NOISE_CANCELLATION_TIME;
#endif

    /* Enable and config the function for RMC channel */
    RMC_Enable(RMCx);
    RMC_SetIdleMode(RMCx, DISABLE);
    RMC_Init(RMCx, &myRMC);
    /* RMC Reception enable */
    RMC_SetRxCtrl(RMCx, ENABLE);

    /* Enable the Receive interrupt for RMC channel */
    /* RMC0 should to enable INTRMCRX0_interrupt, RMC1 should to enable INTRMCRX1_interrupt. */
    NVIC_Enable_Interrupt(INTRMCRX0_interrupt);

    __enable_irq();

    do {
        if (fRMCRx) {
#ifdef DEBUG
			uart_print_str("RMC DATA: 0x");
			uart_print_word(RMC_Command);
			uart_print_str("\n");
#endif
            fRMCRx = None;
        }
    } while (1);
}

void RMC_Configuration(TSB_RMC_TypeDef * RMCx)
{
    if (RMCx == TSB_RMC0) {
        TSB_PE_FR1_PE3F1 = 1;
        TSB_PE_IE_PE3IE = 1;
    } else {
        if (RMCx == TSB_RMC1) {
            TSB_PF_FR1_PF3F1 = 1;
            TSB_PF_IE_PF3IE = 1;
        } else {
            /*Do nothing */
        }
    }
}

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
