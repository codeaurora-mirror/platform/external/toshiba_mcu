/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   the application functions of UART0 to UART2 demo for the TOSHIBA
*          'TMPM330' Device Series 
* @version V1.200
* @date    2010/06/09
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

#include "tmpm330_uart.h"
#include "lcd.h"

#define BufSize (sizeof(TxBuffer) / sizeof(*(TxBuffer)))

typedef enum { KEY1 = 1U, KEY2, KEY3, KEY4 } KEYID;
uint8_t TxBuffer[] = "TMPM330";
uint8_t RxBuffer[BufSize] = { 0U };

const uint8_t NumToBeTx = BufSize - 1U;
uint8_t TxCounter = 0U;
uint8_t RxCounter = 0U;
UART_InitTypeDef myUART;

void SIO_Configuration(TSB_SC_TypeDef * SCx);
void Key_Configuration(void);
void Key_WaitRelease(KEYID key);
void ResetIdx(void);

int main(void)
{
    Key_Configuration();
    LCD_Configuration();
    SIO_Configuration(UART0);
    SIO_Configuration(UART2);

    myUART.BaudRate = 115200U;
    myUART.DataBits = UART_DATA_BITS_8;
    myUART.StopBits = UART_STOP_BITS_1;
    myUART.Parity = UART_NO_PARITY;
    myUART.Mode = UART_ENABLE_RX | UART_ENABLE_TX;
    myUART.FlowCtrl = UART_NONE_FLOW_CTRL;

    UART_Enable(UART0);
    UART_Init(UART0, &myUART);

    UART_Enable(UART2);
    UART_Init(UART2, &myUART);

    LCD_Light(ON);

    NVIC_Enable_Interrupt(INTTX0_interrupt);
    NVIC_Enable_Interrupt(INTRX2_interrupt);

    do {
        Send_To_LCD(FIRST_LINE, LCD_COMMAND);
        Send_LCD_Text("UART0 to UART2");
        Send_To_LCD(SECOND_LINE, LCD_COMMAND);
        Send_LCD_Text("Press K1: Start");
        Key_WaitRelease(KEY1);

        UART_SetTxData(UART0, (uint32_t) (TxBuffer[TxCounter++]));
        while (RxCounter < NumToBeTx) {
            /* Do nothing */
        }
        Send_To_LCD(SECOND_LINE, LCD_COMMAND);
        Send_LCD_Text((char *) RxBuffer);
        Key_WaitRelease(KEY1);
        ResetIdx();
    } while (1);
}

void ResetIdx(void)
{
    uint8_t i = 0U;

    for (i = 0U; i <= RxCounter; i++) {
        RxBuffer[i] = 0U;
    }
    TxCounter = 0U;
    RxCounter = 0U;
}

void Key_Configuration(void)
{
    TSB_PB->IE |= 0xF0U;
    TSB_PB->PUP |= 0xF0U;
}

void Key_WaitRelease(KEYID key)
{
    switch (key) {
    case KEY1:
        while (TSB_PB_DATA_PB7);
        while (!TSB_PB_DATA_PB7);
        break;
    case KEY2:
        while (TSB_PB_DATA_PB6);
        while (!TSB_PB_DATA_PB6);
        break;
    case KEY3:
        while (TSB_PB_DATA_PB5);
        while (!TSB_PB_DATA_PB5);
        break;
    case KEY4:
        while (TSB_PB_DATA_PB4);
        while (!TSB_PB_DATA_PB4);
        break;
    default:
        break;
    }
}

void SIO_Configuration(TSB_SC_TypeDef * SCx)
{
    if (SCx == UART0) {
        TSB_PE_CR_PE0C = 1U;
        TSB_PE_FR1_PE0F1 = 1U;
        TSB_PE_FR1_PE1F1 = 1U;
        TSB_PE_IE_PE1IE = 1U;
    } else if (SCx == UART1) {
        TSB_PE_CR_PE4C = 1U;
        TSB_PE_FR1_PE4F1 = 1U;
        TSB_PE_FR1_PE5F1 = 1U;
        TSB_PE_IE_PE5IE = 1U;
    } else if (SCx == UART2) {
        TSB_PF_CR_PF0C = 1U;
        TSB_PF_FR1_PF0F1 = 1U;
        TSB_PF_FR1_PF1F1 = 1U;
        TSB_PF_IE_PF1IE = 1U;
    } else {
        /* Do nothing */
    }
}

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
