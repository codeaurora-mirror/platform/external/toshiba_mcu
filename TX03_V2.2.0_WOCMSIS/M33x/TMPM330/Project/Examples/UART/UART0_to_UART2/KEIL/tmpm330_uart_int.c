/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    tmpm330_uart_int.c
* @brief   all interrupt request functions definition of UART (Serial Channel)
*          for the TOSHIBA 'TMPM330' Device Series 
* @version V1.200
* @date    2010/05/21
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "tmpm330_uart_int.h"

extern const uint8_t NumToBeTx;
extern uint8_t TxBuffer[];
extern uint8_t RxBuffer[];
extern uint8_t TxCounter;
extern uint8_t RxCounter;

/**
  * @brief  The transmission interrupt request function of UART channel 0.
  * @param  None
  * @retval None
  */
void INTTX0_IRQHandler(void)
{
    volatile UART_Err err;

    if (TxCounter < NumToBeTx) {
        UART_SetTxData(UART0, TxBuffer[TxCounter++]);
    } else {
        err = UART_GetErrState(UART0);
    }
}

/**
  * @brief  The reception interrupt request function of UART channel 2.
  * @param  None
  * @retval None
  */
void INTRX2_IRQHandler(void)
{
    volatile UART_Err err;

    err = UART_GetErrState(UART2);
    if (UART_NO_ERR == err) {
        RxBuffer[RxCounter++] = (uint8_t) UART_GetRxData(UART2);
    }
}
